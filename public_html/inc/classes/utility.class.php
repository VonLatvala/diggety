<?php
    /**
     * Class to hold utility functions.
     *
     * @author Axel Latvala
     * @version 1.0
     * @static
     * Created  31.10.2012 22:48 GMT+2
     * Modified 12.11.2012 02:04 GMT+2, Axel Latvala 
     */
    class utility
    {
        public function __construct()
        {

        }
        
        public static function is_uuid($str)
        {
            $pattern = "/\b[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}\b/";
            if(preg_match($pattern, $str))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public static function generate_uuid()
        {
            $DB = @new DB(ENV_MAIN_MYSQL_HOST, ENV_MAIN_MYSQL_USER, ENV_MAIN_MYSQL_PASSWORD, ENV_MAIN_MYSQL_DB_NAME);
            
            if(!$DB->is_connected())
            {
                trigger_error("Could not connect to the database. <br />\nMySQL(".$DB->error_code()."): ".$DB->error_str(), E_USER_ERROR);
                die();
            }
            
            if($DB->query("SELECT CONV_BIN_TXT_UUID(GEN_BIN_UUID()) as UUID"))
            {
                $resarr = $DB->result->fetch_assoc(); 
                $DB->result->close();
                $DB->disconnect();
                
                return $resarr['UUID'];
            }
            else
            {
                echo $DB->error_str();
                return false;
            }
        }
        
        public static function is_valid_url($str)
        {
            return (bool)parse_url($str);
        }

        public static function is_url_reachable($url)
        {
            if(!self::is_valid_url($url))
            {
                return false;
            }
            
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_NOBODY, TRUE);
            $data = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            
            if($httpcode>=200 && $httpcode<300)
            {
                return true;
            }
            else
            {
                return false;
            }  
        }
        
        public static function url_info($url)
        {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,            $url);
            curl_setopt($ch, CURLOPT_HEADER,         true);
            curl_setopt($ch, CURLOPT_NOBODY,         true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT,        10);

            $r = curl_exec($ch);
            
            $retVal = array();
            $fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $r));
            foreach( $fields as $field )
            {
                if( preg_match('/([^:]+): (.+)/m', $field, $match) )
                {
                    $match[1] = preg_replace('/(?<=^|[\x09\x20\x2D])./e', 'strtoupper("\0")', strtolower(trim($match[1])));
                    if( isset($retVal[$match[1]]) )
                    {
                        $retVal[$match[1]] = array($retVal[$match[1]], $match[2]);
                    }
                    else
                    {
                        $retVal[$match[1]] = trim($match[2]);
                    }
                }
            }
            
            $res['header'] = $retVal;
            return $res;
        }

        public static function is_valid_email_address($str)
        {
            return filter_var($str, FILTER_VALIDATE_EMAIL);
        }
        
        public static function without_spaces($str)
        {
            return implode('', explode(' ', $str));
        }
        
        public static function is_valid_timestamp($int)
        {
            $int = (string)$int;
            return ((string) (int) $int === $int) 
                && ($int <= PHP_INT_MAX)
                && ($int >= ~PHP_INT_MAX);
        }

        public static function random_string($chars)
        {
            $res = "";
            for($i=1;$i<=$chars;$i++)
            {
                $res .= mb_convert_encoding(pack('n', rand(0x61,0x7A)), 'UTF-8', 'UTF-16BE');
            }
            return $res;
        }

        public static function property(&$properties, $property, $bool = null)
        {
            // Mode describes if we are reading or writing. 0 = read, 1 = write.
            if($bool === null)
            {
                $mode = 0;
            }
            else
            {
                $mode = 1;
            }
            if($mode == 1)
            {
                if($bool === false)
                {
                    $replacement = "0";
                }
                else if($bool === true)
                {
                    $replacement = "1";
                }
                else
                {
                    // Invalid input.
                    return false;
                }
                $properties = substr_replace($properties, $replacement, $property, 1);
                return true;
            }
            else
            {
                return substr($properties, $property, 1);
            }
        }
        
        public static function could_be_int($mixed)
        {
            if((int)$mixed == $mixed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        /** 
         * Convert number of seconds into hours, minutes and seconds 
         * and return an array containing those values 
         * 
         * @param integer $inputSeconds Number of seconds to parse 
         * @return array 
         */ 

        public static function secondsToTime($inputSeconds)
        {
            $secondsInAMinute = 60;
            $secondsInAnHour  = 60 * $secondsInAMinute;
            $secondsInADay    = 24 * $secondsInAnHour;

            // extract days
            $days = floor($inputSeconds / $secondsInADay);

            // extract hours
            $hourSeconds = $inputSeconds % $secondsInADay;
            $hours = floor($hourSeconds / $secondsInAnHour);

            // extract minutes
            $minuteSeconds = $hourSeconds % $secondsInAnHour;
            $minutes = floor($minuteSeconds / $secondsInAMinute);

            // extract the remaining seconds
            $remainingSeconds = $minuteSeconds % $secondsInAMinute;
            $seconds = ceil($remainingSeconds);

            // return the final array
            $obj = array(
                'd' => (int) $days,
                'h' => (int) $hours,
                'm' => (int) $minutes,
                's' => (int) $seconds,
            );
            return $obj;
        }

    }

?>
