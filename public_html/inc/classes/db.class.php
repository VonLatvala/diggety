<?php
    require_once 'inc/env_vars.php';
    /**
     * Created  31.10.2012 23:22 GMT+2<br />
     * 
     * DB is a database abstraction abstraction class.
     *
     * @author  Axel Latvala
     * @version 1.0
     */
    class DB
    {
        private $mysqli;
        private $host;
        private $user;
        private $password;
        private $db_name;
        private $connected = false;
        private $errorcode = "0";
        private $errorstring = "Not connected.";
        public $result = false;
        private $affected_rows = false;

        public function __construct($host, $user, $password, $db_name = "")
        {
            if(!empty($host))
                $this->host = $host;
            if(!empty($user))
                $this->user = $user;
            if(!empty($password))
                $this->password = $password;
            if(!empty($db_name))
                $this->db_name = $db_name;
            
            if(!empty($host) && !empty($user) && !empty($password))
                $this->connect();
        }
        
        private function connect()
        {
            if(empty($this->db_name))
            {
                $this->mysqli = @new mysqli($this->host, $this->user, $this->password);
                if(!$this->mysqli->connect_error)
                {
                    $this->connected = true;
                    $this->errorcode = false;
                    $this->errorstring = false;
                }
                else
                {
                    $this->connected = false;
                    $this->errorcode = $this->mysqli->connect_errno;
                    $this->errorstring = $this->mysqli->connect_error;
                }
            }
            else
            {
                $this->mysqli = @new mysqli($this->host, $this->user, $this->password, $this->db_name);
                if(!$this->mysqli->connect_error)
                {
                    $this->connected = true;
                    $this->errorcode = false;
                    $this->errorstring = false;
                }
                else
                {
                    $this->connected = false;
                    $this->errorcode = $this->mysqli->connect_errno;
                    $this->errorstring = $this->mysqli->connect_error;
                }
            }
            // Charset etc.
            if(!$this->mysqli->connect_error)
            {
                $this->mysqli->set_charset('utf8');
            }
        }
        public function query($query)
        {
            if($this->connected === true)
            {
                if($this->result = $this->mysqli->query($query))
                {
                    $this->affected_rows = $this->mysqli->affected_rows;
                    $this->errorcode = false;
                    $this->errorstring = false;
                    return true;
                }
                else
                {
                    $this->errorcode = $this->mysqli->errno;
                    $this->errorstring = $this->mysqli->error;                    
                    return false;
                }
            }
            else
            {
                $this->errorstring = "Not connected.";
                return false;
            }
        }
        
        public function is_connected()
        {
            return $this->connected;
        }
        
        public function error_code()
        {
            return $this->errorcode;
        }

        public function error_str()
        {
            return $this->errorstring;
        }
        
        public function real_escape_string($str)
        {
            return $this->mysqli->real_escape_string($str);
        }

        public function select_db($str)
        {
            $this->mysqli->select_db($str);
            
            if($this->result = $this->mysqli->query("SELECT DATABASE()"))
            {
                $res = $this->result->fetch_row();
                
                $this->result->close();
                
                if($res[0] == $str)
                {
                    return true;
                }
                else
                {
                    $this->errorcode = $this->mysqli->errorno;
                    $this->errorstring = $this->mysqli->error;
                    return false;
                }
            }
            else
            {
                $this->errorcode = $this->mysqli->errorno;
                $this->errorstring = $this->mysqli->error;
                return false;
            }
        }

        public function disconnect()
        {
            if($this->is_connected())
            {
                $this->connected = false;
                $this->mysqli->close();
            }
        }
        
        public function affected_rows()
        {
            return $this->affected_rows;
        }

        public function __destruct()
        {
            if($this->is_connected())
            {
                $this->connected = false;
                $this->mysqli->close();
            }
        }
    }

?>
