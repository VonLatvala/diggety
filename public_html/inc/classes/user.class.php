<?php

    require_once 'inc/env_vars.php';
    require_once 'inc/classes/db.class.php';
    require_once 'inc/classes/utility.class.php';
    /**
     * Created  13.11.2012 20:57 GMT+2<br />
     * Modified 13.11.2012 20:57 GMT+2, Axel Latvala
     * 
     * User is the class which manages the current user.
     *
     * @author  Axel Latvala
     * @version 1.0
     */
    define("USER_DB_COL_USER_ID", "id");
    define("USER_DB_COL_USER_UUID", "user_uuid");
    define("USER_DB_COL_EMAIL", "email");
    define("USER_DB_COL_USERNAME", "username");
    define("USER_DB_COL_FIRST_NAME", "first_name");
    define("USER_DB_COL_LAST_NAME", "last_name");
    define("USER_DB_COL_BIRTHDATE", "birthdate");
    define("USER_DB_COL_GENDER", "gender");
    define("USER_DB_COL_COUNTRY_CODE", "country_code");
    define("USER_DB_COL_PROPERTIES", "properties");
    define("USER_DB_COL_REGISTERED", "registered");
    define("USER_DB_COL_FB_ID", "facebook_id");

    define("USER_STRLEN_USERNAME", 32);
    define("USER_STRLEN_EMAIL", 100);
    define("USER_STRLEN_FIRST_NAME", 100);
    define("USER_STRLEN_LAST_NAME", 100);
    define("USER_STRLEN_COUNTRY_CODE", 3);
    define("USER_STRLEN_PROPERTIES", 20);

    define("USER_FB_APP_ID", "492479304118920");
    define("USER_FB_APP_SECRET", "b552a5c1974777cc742650ea5c2142d8");

    define("PASSWORD_DB_COL_ID", "id");
    define("PASSWORD_DB_COL_USER_UUID", "user_uuid");
    define("PASSWORD_DB_COL_PASSWORD", "password");

    define("USER_DB_TABLE_USER_DETAILS", "user_details");
    define("USER_DB_TABLE_PASSWORD", "password");

    define("USER_RIGHT_COMMENT", 0);
    define("USER_RIGHT_SAVE_POST", 1);
    define("USER_PROPERTY_GENDER", 2);
    define("USER_PROPERTY_VAL_GENDER_FEMALE", 0);
    define("USER_PROPERTY_VAL_GENDER_MALE", 1);
    define("USER_PROPERTY_IS_FACEBOOK_USER", 3);
    define("USER_RIGHT_DEBUG_USER", 9);
    define("USER_PROPERTY_IS_DUMMY_USER", 10);
    define("USER_PROPERTY_IS_SUPERUSER", 11);
    define("USER_RIGHT_MODERATOR", 12);
    define("USER_RIGHT_ADMIN", 13);
    
    class User
    {
        // Data
        private $user_id;       // UNSIGNED BIGINT(20) to hold the auto_incremented ID for the user row.
        private $user_uuid;     // BINARY(16) to hold the user UUID
        private $email;         // VARCHAR(100) to hold the email address of the user
        private $username;      // VARCHAR(32) to hold the username of the user
        private $first_name;    // VARCHAR(100) to hold the first name of the user
        private $last_name;     // VARCHAR(100) to hold the last name of the user
        private $birthdate;     // INT(11) to hold the UNIX timestamp of the users' birth date
        private $country_code;  // VARCHAR(3) to hold the country code
        private $properties;    // VARCHAR(20) to hold the properties
        private $registered;    // INT(11) to hold the registration date as UNIX timestamp
        private $facebook_id = null;
        // State
        private $load_failed;   // Used to determine if loading user data failed.
        private $is_new = true; // Used to determine if this object contains an existing or a new user.
        private $error_msg;
        private $logged_in = false;
        private $data_loaded = false;
        // References
        private $DB;            // The database object (reference)

        public function __construct($id, $DB)
        {
            // Dependencies
            if(!class_exists("utility"))
            {
                trigger_error("Dependency requirements not met. Missing class: utility.", E_USER_ERROR);
                die();
            }
            if(!class_exists("DB"))
            {
                trigger_error("Dependency requirements not met. Missing class: DB.", E_USER_ERROR);
                die();
            }

            $this->DB = $DB;

            if(!$this->DB->is_connected())
            {
                trigger_error("Could not connect to the database. <br />\nMySQL(".$this->DB->error_code()."): ".$this->DB->error_str(), E_USER_ERROR);
                die();
            }

            if(utility::is_uuid($id)) // Are we constructing with a valid UUID? If yes, assume existing user.
            {
                #echo "Valid UUID.";
                if($this->load_data($id))
                {
                    #echo "Data loaded.";
                    // Loading user data succeeded.
                    $this->logged_in = true;
                    $this->load_failed = false;
                    $this->is_new = false;
                }
                else
                {
                    #echo "Failed loading data.";
                    // Loading user data failed.
                    $this->load_failed = true;
                    $this->is_new = false;
                }
            }
            else // We are not constructing with a valid UUID, assume new user.
            {
                #echo "Invalid UUID.";
                $this->load_failed = false;
                $this->is_new = true;
            }

        }

        public function to_array()
        {
            return array
                (
                "logged_in" => $this->logged_in,
                "user_id" => $this->user_id,
                "user_uuid" => $this->user_uuid,
                "email" => $this->email,
                "username" => $this->username,
                "first_name" => $this->first_name,
                "last_name" => $this->last_name,
                "birthdate" => $this->birthdate,
                "gender" => $this->property(USER_PROPERTY_GENDER)==0?"female":"male",
                "country_code" => $this->country_code,
                "registered" => $this->registered,
                "facebook_user" => $this->property(USER_PROPERTY_IS_FACEBOOK_USER)?true:false,
                "is_mod" => $this->property(USER_RIGHT_MODERATOR)?true:false,
                "is_admin" => $this->property(USER_RIGHT_ADMIN)?true:false
            );

        }

        public function do_login($user_name, $password)
        {
            $query = "SELECT CONV_BIN_TXT_UUID(".USER_DB_COL_USER_UUID.") as ".USER_DB_COL_USER_UUID." FROM ".USER_DB_TABLE_USER_DETAILS." WHERE ".USER_DB_COL_EMAIL." = '".$user_name."' OR ".USER_DB_COL_USERNAME." = '".$user_name."'";
            if($this->DB->query($query))
            {
                if($this->DB->result->num_rows<=0)
                {
                    $this->error_msg = "No matching usernames or emails.";
                    return false;
                }
                if($resarr = $this->DB->result->fetch_assoc())
                {
                    if(!empty($resarr[USER_DB_COL_USER_UUID]))
                    {
                        $user_uuid = $resarr[USER_DB_COL_USER_UUID];
                    }
                    else
                    {
                        $this->error_msg = "No matching usernames or emails.";
                        return false;
                    }
                }
                else
                {
                    $this->error_msg = "Error fetching assoc.";
                    return false;
                }
                $this->DB->result->close();
            }
            else
            {
                $this->error_msg = $this->DB->error_code().": ".$this->DB->error_str()."\n".$query;
                return false;
            }
            $query = "SELECT COUNT(*) FROM ".USER_DB_TABLE_PASSWORD." WHERE ".PASSWORD_DB_COL_PASSWORD." = SHA1('".$password."') AND ".PASSWORD_DB_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$user_uuid."')";
            if($this->DB->query($query))
            {
                if($resarr = $this->DB->result->fetch_row())
                {
                    if($resarr[0]==0&&$resarr[0]!==false)
                    {
                        $this->error_msg = "Login failed. Wrong password.";
                        return false;
                    }
                    else if($resarr[0]==1)
                    {
                        $_SESSION['user_uuid'] = $user_uuid;
                        $this->load_data($user_uuid);
                        $this->error_msg = null;
                        $this->logged_in = true;
                        return true;
                    }
                    else
                    {
                        $this->error_msg = "Internal error.";
                        return false;
                    }
                }
                else
                {
                    $this->error_msg = "Error fetching assoc.";
                    return false;
                }
            }
            else
            {
                $this->error_msg = $this->DB->error_code().": ".$this->DB->error_str()."\n".$query;
                return false;
            }

        }

        public function facebook_id_login($fb_id)
        {
            $user_uuid = $this->get_user_uuid_fb_id($fb_id);
            if(!$user_uuid)
            {
                return false;
            }
            if(!$this->load_data($user_uuid))
            {
                return false;
            }
            $_SESSION['user_uuid'] = $user_uuid;
            $this->logged_in = true;
            return true;
        }

        public function facebook_challenge_url()
        {
            $my_url = "https://".$_SERVER['HTTP_HOST']."/api.php?q=fb_challenge_callback";
            
            $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
            
            $dialog_url = "https://www.facebook.com/dialog/oauth?client_id=".USER_FB_APP_ID."&redirect_uri=".urlencode($my_url)."&state=".$_SESSION['state']."&scope=user_birthday,user_hometown,user_location,email";
            echo $dialog_url;
        }
        
        public function facebook_challenge_callback()
        {
            $code = $_REQUEST['code'];
            if($_SESSION['state']&&($_SESSION['state']===$_REQUEST['state']))
            {
                $token_url = "https://graph.facebook.com/oauth/access_token?"
                        ."client_id=".USER_FB_APP_ID."&redirect_uri=".urlencode($my_url)
                        ."&client_secret=".USER_FB_APP_SECRET."&code=".$code;

                $response = file_get_contents($token_url);
                $params = null;

                parse_str($response, $params);
                $_SESSION['access_token'] = $params['access_token'];

                $graph_url = "https://graph.facebook.com/me?access_token=".$params['access_token'];

                $user = json_decode(file_get_contents($graph_url));

                if($this->is_facebook_user($user->id))
                {
                    if($this->error_msg==null)
                    {
                        $user_uuid = $this->get_user_uuid_fb_id($user->id);
                        $_SESSION['user_uuid'] = $user_uuid;
                        if($this->load_data($user_uuid))
                        {
                            $this->error_msg = null;
                            $this->logged_in = true;
                            return true;
                        }
                        else
                        {
                            $this->logged_in = false;
                            return false;
                        }
                    }
                    else
                    {
                        // FAILED CHECKING IF IS FB USER
                        $this->logged_in = false;
                        return false;
                    }
                }
                else
                {
                    // Register
                    $this->user_uuid = utility::generate_uuid();
                    $this->email = $user->email;
                    $this->first_name = $user->first_name;
                    $this->last_name = $user->last_name;
                    do
                    {
                        $res = $this->user_exists_username($user->username);
                        if($res[0]==true)
                        {
                            if($res[1]==false)
                            {
                                // User does not exist.
                                $unique = true;
                            }
                            else
                            {
                                // User exists.
                                $unique = false;
                                $user->username = $user->username.rand(0, 9999);
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    while($unique==false);
                    $this->username = $user->username;
                    $this->birthdate = strtotime($user->birthday);
                    $tmp = explode("_", $user->locale);
                    $this->country_code = $tmp[1];
                    if(empty($this->properties))
                    {
                        $this->properties = "";
                        for($i = 1; $i>=USER_STRLEN_PROPERTIES; $i++)
                        {
                            $this->properties .= "0";
                        }
                    }
                    utility::property($this->properties, USER_PROPERTY_GENDER, ($user->gender=="male")?true:false);
                    utility::property($this->properties, USER_RIGHT_SAVE_POST, true);
                    utility::property($this->properties, USER_RIGHT_COMMENT, true);
                    utility::property($this->properties, USER_PROPERTY_IS_FACEBOOK_USER, true);
                    $this->registered = time();
                    $this->facebook_id = $user->id;
                    $this->is_new = true;
                    if($this->save_data())
                    {
                        if($this->load_data($this->user_uuid))
                        {
                            $_SESSION['user_uuid'] = $this->user_uuid;
                            $this->error_msg = null;
                            $this->logged_in = true;
                            return true;
                        }
                        else
                        {
                            $this->logged_in = false;
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public function facebook_authorize()
        {
            $code = $_REQUEST['code'];
            $my_url = "https://".$_SERVER['HTTP_HOST']."/";
            if(empty($code))
            {
                $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
                $dialog_url = "https://www.facebook.com/dialog/oauth?client_id="
                        .USER_FB_APP_ID."&redirect_uri=".urlencode($my_url)."&state="
                        .$_SESSION['state']."&scope=user_birthday,user_hometown,user_location,email";
                header("Location: ".$dialog_url);
                echo '<html><head><script>top.location.href=\''.$dialog_url.'\'</script></head><body></body></html>';
            }
            else
            {
                if($_SESSION['state']&&($_SESSION['state']===$_REQUEST['state']))
                {
                    $token_url = "https://graph.facebook.com/oauth/access_token?"
                            ."client_id=".USER_FB_APP_ID."&redirect_uri=".urlencode($my_url)
                            ."&client_secret=".USER_FB_APP_SECRET."&code=".$code;

                    $response = file_get_contents($token_url);
                    $params = null;

                    parse_str($response, $params);
                    $_SESSION['access_token'] = $params['access_token'];

                    $graph_url = "https://graph.facebook.com/me?access_token="
                            .$params['access_token'];

                    $user = json_decode(file_get_contents($graph_url));

                    if($this->is_facebook_user($user->id))
                    {
                        if($this->error_msg==null)
                        {
                            $user_uuid = $this->get_user_uuid_fb_id($user->id);
                            $_SESSION['user_uuid'] = $user_uuid;
                            if($this->load_data($user_uuid))
                            {
                                $this->error_msg = null;
                                $this->logged_in = true;
                                return true;
                            }
                            else
                            {
                                $this->logged_in = false;
                                return false;
                            }
                        }
                        else
                        {
                            // FAILED CHECKING IF IS FB USER
                            $this->logged_in = false;
                            return false;
                        }
                    }
                    else
                    {
                        // Register
                        $this->user_uuid = utility::generate_uuid();
                        $this->email = $user->email;
                        $this->first_name = $user->first_name;
                        $this->last_name = $user->last_name;
                        do
                        {
                            $res = $this->user_exists_username($user->username);
                            if($res[0]==true)
                            {
                                if($res[1]==false)
                                {
                                    // User does not exist.
                                    $unique = true;
                                }
                                else
                                {
                                    // User exists.
                                    $unique = false;
                                    $user->username = $user->username.rand(0, 9999);
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                        while($unique==false);
                        $this->username = $user->username;
                        $this->birthdate = strtotime($user->birthday);
                        $tmp = explode("_", $user->locale);
                        $this->country_code = $tmp[1];
                        if(empty($this->properties))
                        {
                            $this->properties = "";
                            for($i = 1; $i>=USER_STRLEN_PROPERTIES; $i++)
                            {
                                $this->properties .= "0";
                            }
                        }
                        utility::property($this->properties, USER_PROPERTY_GENDER, ($user->gender=="male")?true:false);
                        utility::property($this->properties, USER_RIGHT_SAVE_POST, true);
                        utility::property($this->properties, USER_RIGHT_COMMENT, true);
                        utility::property($this->properties, USER_PROPERTY_IS_FACEBOOK_USER, true);
                        $this->registered = time();
                        $this->facebook_id = $user->id;
                        #echo json_encode(array("diggety_user" => $this->to_array(), "fb_response" => $user));
                        $this->is_new = true;
                        if($this->save_data())
                        {
                            if($this->load_data($this->user_uuid))
                            {
                                $_SESSION['user_uuid'] = $this->user_uuid;
                                $this->error_msg = null;
                                $this->logged_in = true;
                                return true;
                            }
                            else
                            {
                                $this->logged_in = false;
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

        }

        public function load_data($user_uuid)
        {
            $query = "".
                    "SELECT * FROM ".USER_DB_TABLE_USER_DETAILS." WHERE ".
                    USER_DB_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$user_uuid."')";
            if($this->DB->query($query))
            {
                $resarr = $this->DB->result->fetch_assoc();
                $this->DB->result->close();

                $this->data_loaded = true;
                $this->load_failed = false;

                $this->user_id = $resarr[USER_DB_COL_USER_ID];
                $this->user_uuid = $user_uuid;
                $this->email = $resarr[USER_DB_COL_EMAIL];
                $this->username = $resarr[USER_DB_COL_USERNAME];
                $this->first_name = $resarr[USER_DB_COL_FIRST_NAME];
                $this->last_name = $resarr[USER_DB_COL_LAST_NAME];
                $this->birthdate = $resarr[USER_DB_COL_BIRTHDATE];
                $this->country_code = $resarr[USER_DB_COL_COUNTRY_CODE];
                $this->properties = $resarr[USER_DB_COL_PROPERTIES];
                $this->registered = $resarr[USER_DB_COL_REGISTERED];
                return true;
            }
            else
            {
                $this->data_loaded = false;
                $this->load_failed = true;
                $this->error_msg = $this->DB->error_code().": ".$this->DB->error_str()."\n".$query;
                return false;
            }

        }

        public function property($property, $bool = null)
        {
            if(strlen($this->properties) != USER_STRLEN_PROPERTIES)
            {
                $this->properties = "";
                for($i=1;$i<=USER_STRLEN_PROPERTIES;$i++)
                {
                    $this->properties .= "0";
                }
            }
            return utility::property($this->properties, $property, $bool);
        }

        public function logged_in()
        {
            return $this->logged_in;

        }

        public function log_out()
        {
            session_destroy();
            $_SESSION = array();

        }

        public function error_message()
        {
            // Read only
            return $this->error_msg;

        }

        private function is_facebook_user($fb_id)
        {
            if(!is_numeric($fb_id))
            {
                $this->error_msg = "Facebook ID has to be numeric.";
                return false;
            }
            $query = "SELECT COUNT(*) as count FROM ".USER_DB_TABLE_USER_DETAILS." WHERE ".USER_DB_COL_FB_ID." = ".$fb_id;
            if($this->DB->query($query))
            {
                $this->error_msg = null;
                $resarr = $this->DB->result->fetch_assoc();
                $this->DB->result->close();
                if($resarr['count']>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error_msg = $this->DB->error_code().": ".$this->DB->error_str()."\n".$query;
                return false;
            }

        }

        private function get_user_uuid_fb_id($fb_id)
        {
            if(!is_numeric($fb_id))
            {
                $this->error_msg = "Facebook ID has to be numeric.";
                return false;
            }
            $query = "SELECT CONV_BIN_TXT_UUID(".USER_DB_COL_USER_UUID.") as ".USER_DB_COL_USER_UUID." FROM ".USER_DB_TABLE_USER_DETAILS." WHERE ".USER_DB_COL_FB_ID." = ".$fb_id;
            if($this->DB->query($query))
            {
                if($this->DB->affected_rows>0)
                {
                    $resarr = $this->DB->result->fetch_assoc();
                    $this->DB->result->close();
                    $this->error_msg = null;
                    return $resarr[USER_DB_COL_USER_UUID];
                }
                else
                {
                    $this->error_msg = "Facebook user not found.";
                    return false;
                }
            }
            else
            {
                $this->error_msg = $this->DB->error_code().": ".$this->DB->error_str()."\n".$query;
                return false;
            }

        }

        public function save_data()
        {
            if($this->is_new)
            {
                if(!utility::is_uuid($this->user_uuid))
                {
                    $this->user_uuid = utility::generate_uuid();
                }
                if(strlen($this->properties!=USER_STRLEN_PROPERTIES))
                {
                    for(; strlen($this->properties)<USER_STRLEN_PROPERTIES;)
                    {
                        $this->properties .= "0";
                    }
                }
                if(!is_numeric($this->birthdate))
                {
                    $this->birthdate = 0;
                }
                do
                {
                    $res = $this->user_exists_uuid($this->user_uuid);
                    if($res[0]==true)
                    {
                        if($res[1]==false)
                        {
                            // User does not exist.
                            $unique = true;
                        }
                        else
                        {
                            // User exists.
                            $unique = false;
                            $this->user_uuid = utility::generate_uuid();
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                while($unique==false);
                $query = "INSERT INTO ".
                        USER_DB_TABLE_USER_DETAILS
                        ." (".
                        USER_DB_COL_USER_UUID.", ".
                        USER_DB_COL_EMAIL.", ".
                        USER_DB_COL_USERNAME.", ".
                        USER_DB_COL_FIRST_NAME.", ".
                        USER_DB_COL_LAST_NAME.", ".
                        USER_DB_COL_BIRTHDATE.", ".
                        USER_DB_COL_COUNTRY_CODE.", ".
                        USER_DB_COL_PROPERTIES.", ".
                        USER_DB_COL_REGISTERED.", ".
                        USER_DB_COL_FB_ID
                        .") VALUES (".
                        "CONV_TXT_BIN_UUID('".$this->user_uuid."'), ".
                        "'".$this->DB->real_escape_string($this->email)."', ".
                        "'".$this->DB->real_escape_string($this->username)."', ".
                        "'".$this->DB->real_escape_string($this->first_name)."', ".
                        "'".$this->DB->real_escape_string($this->last_name)."', ".
                        "".$this->birthdate.", ".
                        "'".$this->DB->real_escape_string($this->country_code)."', ".
                        "'".$this->DB->real_escape_string($this->properties)."', ".
                        "".time().", ".
                        "".($this->facebook_id!=null&&is_numeric($this->facebook_id)?$this->facebook_id:"NULL")."".
                        ")";
                if($this->DB->query($query))
                {
                    $this->is_new = false;
                    return true;
                }
                else
                {
                    $this->error_msg = $this->DB->error_code().": ".$this->DB->error_str()."\n".$query;
                    return false;
                }
            }
            else
            {
                if(!utility::is_uuid($this->user_uuid))
                {
                    $this->error_msg = "Invalid user UUID.";
                    return false;
                }
                if(strlen($this->properties!=USER_PROPERTIES_LENGTH))
                {
                    for(; strlen($this->properties)<USER_PROPERTIES_LENGTH;)
                    {
                        $this->properties .= "0";
                    }
                }
                if(!is_numeric($this->birthdate))
                {
                    $this->error_msg = "Birth date has to be numeric.";
                    return false;
                }
                $query = "UPDATE ".USER_DB_TABLE_USER_DETAILS
                        ." SET ".
                        USER_DB_COL_EMAIL."='".$this->DB->real_escape_string($this->email)."', ".
                        USER_DB_COL_USERNAME."='".$this->DB->real_escape_string($this->username)."', ".
                        USER_DB_COL_FIRST_NAME."='".$this->DB->real_escape_string($this->first_name)."', ".
                        USER_DB_COL_LAST_NAME."='".$this->DB->real_escape_string($this->last_name)."', ".
                        USER_DB_COL_BIRTHDATE."=".$this->birthdate.", ".
                        USER_DB_COL_COUNTRY_CODE."='".$this->DB->real_escape_string($this->country_code)."', ".
                        USER_DB_COL_PROPERTIES."='".$this->DB->real_escape_string($this->properties)."', ".
                        USER_DB_COL_REGISTERED."=".$this->registered.", ".
                        USER_DB_COL_FB_ID."=".($this->facebook_id!=null&&is_numeric($this->facebook_id)?$this->facebook_id:"NULL").
                        " WHERE ".
                        USER_DB_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$this->user_uuid."')";
                if($this->DB->query($query))
                {
                    return true;
                }
                else
                {
                    $this->error_msg = $this->DB->error_code().": ".$this->DB->error_str()."\n".$query;
                    return false;
                }
            }

        }

        public function load_failed()
        {
            return $this->load_failed;

        }

        public function user_id()
        {
            return $this->user_id;

        }

        public function user_uuid()
        {
            return $this->user_uuid;

        }

        public function email($email = null)
        {
            if(!is_null($email))
            {
                if(utility::is_valid_email_address($email))
                {
                    $this->email = $email;
                    return true;
                }
                else
                {
                    $this->error_msg = "Invalid email address.";
                    return false;
                }
            }
            return $this->email;

        }

        public function username($username = null)
        {
            if(!is_null($username))
            {
                $res = $this->user_exists_username($username);
                $allowed_username_regex = "/\b[\p{L}0-9]{3,".USER_STRLEN_USERNAME."}\b/";
                if(strlen($this->DB->real_escape_string($username))>USER_STRLEN_USERNAME)
                {
                    $this->error_msg = "Username too long. Max length: ".USER_STRLEN_USERNAME;
                    return false;
                }
                elseif(!(preg_match($allowed_username_regex, $username)==1))
                {
                    $this->error_msg = "Username contains illegal characters. Please only use letters and numbers. Faulty username: ".$username;
                    return false;
                }
                elseif($res[0] == true && $res[1] == true)
                {
                    $this->error_msg = "Username taken.";
                    return false;             
                }
                elseif($res[0] == false)
                {
                    $this->error_msg = "Error checking username. ".$this->DB->error_str();
                    return false;
                }
                else
                {
                    $this->username = $username;
                    return true;
                }
            }
            return $this->username;

        }

        public function first_name($first_name = null)
        {
            if(!is_null($first_name))
            {
                $allowed_firstname_regex = "/\b\p{L}{2,".USER_STRLEN_FIRST_NAME."}\b/";
                if(strlen($this->DB->real_escape_string($first_name))>USER_STRLEN_FIRST_NAME)
                {
                    $this->error_msg = "Firstname is too long. Max length: ".USER_STRLEN_FIRST_NAME;
                    return false;
                }
                else if(!preg_match($allowed_firstname_regex, utility::without_spaces($first_name)))
                {
                    $this->error_msg = "Firstname contains illegal characters. Please only use letters. Faulty firstname: ".$first_name;
                    return false;
                }
                else
                {
                    $this->first_name = $first_name;
                    return true;
                }
            }
            return $this->first_name;

        }

        public function last_name($last_name = null)
        {
            if(!is_null($last_name))
            {
                $allowed_lastname_regex = "/\b\p{L}{2,".USER_STRLEN_LAST_NAME."}\b/";
                if(strlen($this->DB->real_escape_string($last_name))>USER_STRLEN_LAST_NAME)
                {
                    $this->error_msg = "Lasttname is too long. Max length: ".USER_STRLEN_LAST_NAME;
                    return false;
                }
                else if(!preg_match($allowed_lastname_regex, utility::without_spaces($last_name)))
                {
                    $this->error_msg = "Lastname contains illegal characters. Please only use letters.";
                    return false;
                }
                else
                {
                    $this->last_name = $last_name;
                    return true;
                }
            }
            return $this->last_name;

        }

        public function birthdate($birthdate = null)
        {
            if(!is_null($birthdate))
            {
                if(utility::is_valid_timestamp($birthdate))
                {
                    $this->birthdate = $birthdate;
                    return true;
                }
                else
                {
                    $this->error_msg = "Invalid birth date.";
                    return false;
                }
            }
            return $this->birthdate;

        }

        public function country_code($country_code = null)
        {
            if(!is_null($country_code))
            {
                $rgx = "/\b[A-Za-z]{2,3}\b/";
                if(!(preg_match($rgx, $country_code)==1))
                {
                    $this->error_msg = "Country code should consist of 2-3 upper or lowercase english letters.";
                    return false;
                }
                else
                {
                    $this->country_code = $country_code;
                    return true;
                }
            }
            return $this->country_code;

        }

        public function properties($properties = null)
        {
            if(!is_null($properties))
            {
                $rgx = "/\b[0-1]{".USER_STRLEN_PROPERTIES."}\b/";
                if(!(preg_match($rgx, $country_code)==1))
                {
                    $this->error_msg = "Invalid properties string.";
                    return false;
                }
                else
                {
                    $this->properties = $properties;
                    return true;
                }
            }
            return $this->properties;

        }

        public function registered()
        {
            return $this->registered;

        }

        public function facebook_id()
        {
            return $this->facebook_id;

        }
        
        public function user_exists_uuid($uuid)
        {
            if(utility::is_uuid($uuid))
            {
                $query = "SELECT COUNT(*) as matches FROM ".USER_DB_TABLE_USER_DETAILS." WHERE ".USER_DB_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$uuid."')";
                if($this->DB->query($query))
                {
                    $resarr = $this->DB->result->fetch_assoc();
                    $this->DB->result->close();
                    if($resarr['matches']>0)
                    {
                        return array(true, true);
                    }
                    else
                    {
                        return array(true, false);
                    }
                }
                else
                {
                    return array(false, NULL);
                }
            }
            else
            {
                $this->error_msg = "Not valid UUID.";
                return array(false, "Not valid UUID.");
            }
        }

        public function user_exists_username($username)
        {
            $username = $this->DB->real_escape_string($username);
            $query = "SELECT COUNT(*) as matches FROM ".USER_DB_TABLE_USER_DETAILS." WHERE ".USER_DB_COL_USERNAME." = '".$username."'";
            if($this->DB->query($query))
            {
                $resarr = $this->DB->result->fetch_assoc();
                $this->DB->result->close();
                if($resarr['matches']>0)
                {
                    return array(true, true);
                }
                else
                {
                    return array(true, false);
                }
            }
            else
            {
                return array(false, NULL);
            }
        }
        
        public function user_exists_email($email)
        {
            $email = $this->DB->real_escape_string($email);
            $query = "SELECT COUNT(*) as matches FROM ".USER_DB_TABLE_USER_DETAILS." WHERE ".USER_DB_COL_EMAIL." = '".$email."'";
            if($this->DB->query($query))
            {
                $resarr = $this->DB->result->fetch_assoc();
                $this->DB->result->close();
                if($resarr['matches']>0)
                {
                    return array(true, true);
                }
                else
                {
                    return array(true, false);
                }
            }
            else
            {
                return array(false, NULL);
            }            
        }


        public function set_password($password)
        {
            if(!$this->is_new)
            {
                $query = "DELETE FROM `password` WHERE user_uuid = CONV_TXT_BIN_UUID('".$this->user_uuid."') LIMIT 1";
                if($this->DB->query($query))
                {
                    
                }
                else
                {
                    $this->error_msg = $this->DB->error_str();
                    return false;
                }
                $query = "INSERT INTO `password` (user_uuid, `password`) VALUES (CONV_TXT_BIN_UUID('".$this->user_uuid()."'), SHA1('".$this->DB->real_escape_string($password)."'))";
                if($this->DB->query($query))
                {
                    return true;
                }
                else
                {
                    $this->error_msg = $this->DB->error_str();
                    return false;
                }
            }
            else
            {
                $this->error_msg = "User needs to be saved first.";
                return false;
            }
        }
    }
?>
