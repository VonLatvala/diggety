<?php
    require_once 'inc/env_vars.php';
    require_once 'inc/classes/db.class.php';
    require_once 'inc/classes/utility.class.php';
    require_once 'inc/classes/user.class.php';
    
    /**
     * the Post Class is used to modify, store and display all data related to a post.
     *
     * @author Axel Latvala
     * @version 1.0
     * 
     * Created  31.10.2012 11:55 GMT+2
     * Modified 12.11.2012 02:04 GMT+2, Axel Latvala
     */
    
    // Definitions
    define("POST_PROPERTY_LIVE", 0);
    define("POST_PROPERTY_REMOVED", 1);
    define("POST_PROPERTY_NSFW", 2);
    define("POST_PROPERTY_NSFL", 3);
    define("POST_PROPERTY_NSFM", 4);
    define("POST_PROPERTY_PI", 5);
    define("POST_PROPERTY_MODIFIED", 6);
    define("POST_PROPERTY_MEME", 6);
    
    define("POST_STRLEN_TITLE", 300);
    define("POST_STRLEN_LINK", 150);
    define("POST_STRLEN_PROPERTIES", 20);
    
    define("POST_DB_COL_POST_ID", "id");
    define("POST_DB_COL_POST_UUID", "post_uuid");
    define("POST_DB_COL_CREATED", "created");
    define("POST_DB_COL_EDITED", "edited");
    define("POST_DB_COL_OWNER_UUID", "owner_uuid");
    define("POST_DB_COL_TITLE", "title");
    define("POST_DB_COL_LINK", "link");
    define("POST_DB_COL_PROPERTIES", "properties");
    
    define("VOTE_DB_COL_ID", "id");
    define("VOTE_DB_COL_POST_UUID", "post_uuid");
    define("VOTE_DB_COL_USER_UUID", "user_uuid");
    define("VOTE_DB_COL_VOTE", "vote");
    define("VOTE_DB_COL_TIMESTAMP", 'timestamp');
    
    define("POST_VOTE_VAL_UPVOTED", 1);
    define("POST_VOTE_VAL_DOWNVOTED", 0);
    
    define("POST_DB_TABLE_POSTS", "posts");
    define("POST_DB_TABLE_POST_VOTES", "post_votes");
    
    class Post
    {
        // Configuration
        private function construct_load_data_query($id)
        {
            $id = $this->DB->real_escape_string($id);
            #echo "SELECT ".POST_DB_COL_POST_ID." as ".POST_DB_COL_POST_ID.", CONV_BIN_TXT_UUID(".POST_DB_COL_POST_UUID.") as ".POST_DB_COL_POST_UUID.", CONV_BIN_TXT_UUID(".POST_DB_COL_OWNER_UUID.") as ".POST_DB_COL_OWNER_UUID.", ".POST_DB_COL_CREATED.", ".POST_DB_COL_EDITED.", ".POST_DB_COL_TITLE.", ".POST_DB_COL_LINK.", ".POST_DB_COL_PROPERTIES." FROM ".POST_DB_TABLE_POSTS." WHERE ".POST_DB_COL_POST_UUID." = CONV_TXT_BIN_UUID('$id')";
            return "SELECT ".POST_DB_COL_POST_ID." as ".POST_DB_COL_POST_ID.", CONV_BIN_TXT_UUID(".POST_DB_COL_POST_UUID.") as ".POST_DB_COL_POST_UUID.", CONV_BIN_TXT_UUID(".POST_DB_COL_OWNER_UUID.") as ".POST_DB_COL_OWNER_UUID.", ".POST_DB_COL_CREATED.", ".POST_DB_COL_EDITED.", ".POST_DB_COL_TITLE.", ".POST_DB_COL_LINK.", ".POST_DB_COL_PROPERTIES." FROM ".POST_DB_TABLE_POSTS." WHERE ".POST_DB_COL_POST_UUID." = CONV_TXT_BIN_UUID('$id')";
        }
        
        private function construct_save_data_query()
        {
            if($this->is_new)
            {
                return "".
                "INSERT INTO ".POST_DB_TABLE_POSTS.
                    "( ".
                        POST_DB_COL_POST_UUID.", ".
                        POST_DB_COL_OWNER_UUID.", ".
                        POST_DB_COL_CREATED.", ".
                        POST_DB_COL_EDITED.", ".
                        POST_DB_COL_TITLE.", ".
                        POST_DB_COL_LINK.", ".
                        POST_DB_COL_PROPERTIES." ".
                    ") ".
                    "VALUES ".
                    "( ".
                        "CONV_TXT_BIN_UUID('".$this->post_uuid."'), ". 
                        "CONV_TXT_BIN_UUID('".$this->owner_uuid."'), ".
                        $this->DB->real_escape_string($this->created).", ".
                        $this->DB->real_escape_string($this->edited).", ".
                        "'".$this->DB->real_escape_string($this->title)."', ".
                        "'".$this->DB->real_escape_string($this->link)."', ".
                        "'".$this->DB->real_escape_string($this->properties)."' ".
                    ")";
            }
            else
            {
                return "".
                "UPDATE ".POST_DB_TABLE_POSTS." ".
                    "SET ".
                    POST_DB_COL_OWNER_UUID." = CONV_TXT_BIN_UUID('".$this->owner_uuid."'), ".
                    POST_DB_COL_CREATED." = ".$this->DB->real_escape_string($this->created).", ".
                    POST_DB_COL_EDITED." = ".$this->DB->real_escape_string($this->edited).", ".
                    POST_DB_COL_TITLE." = '".$this->DB->real_escape_string($this->title)."', ".
                    POST_DB_COL_LINK." = '".$this->DB->real_escape_string($this->link)."', ".
                    POST_DB_COL_PROPERTIES." = '".$this->DB->real_escape_string($this->properties). "' ".
                    "WHERE ".
                    POST_DB_COL_POST_UUID." = CONV_TXT_BIN_UUID('".$this->post_uuid."')";
            }
        }
        // DB Data
        private $post_id;       // UNSIGNED BIGINT To hold post ID
        private $post_uuid;     // BINARY(16) to hold the post UUID
        private $created;       // UNSIGNED INT to hold UNIX Timestamp when the post was created.
        private $edited;        // UNSIGNED INT to hold UNIX Timestamp when the post was edited last time.
        private $owner_uuid;    // BINARY(16) to hold the UUID of the post creator.
        private $title;         // VARCHAR(300) to hold the post title.
        private $link;          // VARCHAR(150) to hold the post link.
        private $properties;    // VARCHAR(20) to hold the bitmask for post properties.

        // Non-DB data
        private $userupvoted;    // bool
        private $user_downvoted; // bool
        private $upvotes;
        private $downvotes;

        // Properties
        private $is_new;        // Used to determine wether the construct function was able to find a post with the supplied id. If not, we assume a new post.
        private $load_failed = true;// Used to determine werher loading the data from the database was successful.
        private $last_error_msg;// Error message container.
        
        // References
        private $DB;
        private $user_ref;


        public function __construct($db_ref, $id = NULL, $user_ref = NULL)
        {
            // Dependencies
            if(!class_exists("utility"))
            {
                trigger_error("Dependency requirements not met. Missing class: utility.", E_USER_ERROR);
                die();
            }
            if(!class_exists("DB"))
            {
                trigger_error("Dependency requirements not met. Missing class: DB.", E_USER_ERROR);
                die();
            }
            if(!class_exists("User"))
            {
                trigger_error("Dependency requirements not met. Missing class: User.", E_USER_ERROR);
                die();
            }
            
            $this->user_ref = $user_ref;
            
            $this->DB = $db_ref;
            
            if(!$this->DB->is_connected())
            {
                trigger_error("Could not connect to the database. <br />\nMySQL(".$this->DB->error_code()."): ".$this->DB->error_str(), E_USER_ERROR);
                die();
            }
            
            $this->properties = '';
            for($i=1;$i<=POST_STRLEN_PROPERTIES;$i++)
            {
                $this->properties .= '0';
            }
            
            if(utility::is_uuid($id)) // Are we constructing with a valid UUID? If yes, assume existing post.
            {
                if($this->load_data($id))
                {
                    // Loading post data succeeded.
                    $this->load_failed = false;
                    $this->is_new = false;
                }
                else
                {
                    // Loading post data failed.
                    $this->load_failed = true;
                    $this->is_new = false;
                }
            }
            else // We are not constructing with a valid UUID, assume new, empty post.
            {
                $this->load_failed = false;
                $this->is_new = true;
                $this->created = time();
                $this->edited = time();
            }
        }
        
        public function load_data($id)
        {
            if(utility::is_uuid($id)) // Is this a valid UUID?
            {
                if($this->DB->query($this->construct_load_data_query($id)))
                {
                    // Store result and free it.
                    $resarr = $this->DB->result->fetch_assoc();
                    $this->DB->result->close();
                    $this->post_id = $resarr[POST_DB_COL_POST_ID];
                    $this->post_uuid = $resarr[POST_DB_COL_POST_UUID];
                    $this->owner_uuid = $resarr[POST_DB_COL_OWNER_UUID];
                    $this->created = $resarr[POST_DB_COL_CREATED];
                    $this->edited = $resarr[POST_DB_COL_EDITED];
                    $this->title = $resarr[POST_DB_COL_TITLE];
                    $this->link = $resarr[POST_DB_COL_LINK];
                    $this->properties = $resarr[POST_DB_COL_PROPERTIES];
                    if($this->load_user_votes() && $this->load_post_votes())
                    {
                        // Loading post data succeeded.
                        $this->load_failed = false;
                        $this->is_new = false;
                        return true;
                    }
                    else
                    {
                        // Loading post data succeeded.
                        $this->load_failed = true;
                        $this->is_new = false;
                        return false;
                    }
                }
                else
                {
                    // Failed loading post data.
                    return false;
                }
            }
            else
            {
                // Invalid input (UUID)
                $this->last_error_msg = "Invalid UUID.";
                return false;
            }
            return true;
        }
        
        public function post_id()
        {
            // Read-Only
            return $this->post_id;
        }
        public function post_uuid()
        {
            // Read-Only
            return $this->post_uuid;
        }
        public function created()
        {
            // Read-Only
            return $this->created;
        }
        public function edited()
        {
            // Read-Only
            return $this->created;
        }
        public function owner_uuid($uuid = null)
        {
            if(!is_null($uuid))
            {
                if(utility::is_uuid($uuid))
                {
                    $this->owner_uuid = $uuid;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return $this->owner_uuid;
            }
        }
        public function title($str = null)
        {
            if(!is_null($str))
            {
                if(strlen($str) > POST_STRLEN_TITLE)
                {
                    $this->title = substr($str, 0, POST_STRLEN_TITLE);
                    return 2;
                }
                else
                {
                    $this->title = $str;
                    return true;
                }
            }
            else
            {
                return $this->title;
            }
        }
        public function link($str = NULL)
        {
            if(!is_null($str))
            {
                $rgx = '/https:\/\/|http:\/\//';
                if(!preg_match($rgx, $str))
                {
                    $str = 'http://'.$str;
                }
                if(!utility::is_valid_url($str))
                {
                    $this->last_error_msg = "Invalid URL.";
                    return false;
                }
                if(!utility::is_url_reachable($str))
                {
                    $this->last_error_msg = "Can't reach URL.";
                    return false;                    
                }
                if(strlen($this->DB->real_escape_string($str)) > POST_STRLEN_LINK)
                {
                    // Do not cut off links. Return false, too long link.
                    $this->last_error_msg = "Too long link. Max length: ".POST_STRLEN_LINK;
                    return false;
                }
                else
                {
                    $this->link = $str;
                    return true;
                }
            }
            else
            {
                if(!$this->user_ref->logged_in() && $this->property(POST_PROPERTY_NSFW))
                {
                    return '//'.$_SERVER['HTTP_HOST']."/images/NSFW_1k.png";
                }
                else
                {
                    return $this->link;
                }
            }
        }
        public function properties()
        {
            return $this->properties;
        }
        
        public function load_failed()
        {
            return $this->load_failed;
        }
        
        public function is_new()
        {
            return $this->is_new;
        }

        public function property($property, $bool = null)
        {
            if(strlen($this->properties) != USER_STRLEN_PROPERTIES)
            {
                $this->properties = "";
                for($i=1;$i>=USER_STRLEN_PROPERTIES;$i++)
                {
                    $this->properties .= "0";
                }
            }
            return utility::property($this->properties, $property, $bool);
        }
        
        private function property_name($int)
        {
            switch($int)
            {
                case POST_PROPERTY_LIVE:
                    return "live";
                    break;
                case POST_PROPERTY_REMOVED:
                    return "removed";
                    break;
                case POST_PROPERTY_NSFW:
                    return "nsfw";
                    break;
                case POST_PROPERTY_NSFL:
                    return "nsfl";
                    break;
                case POST_PROPERTY_NSFM:
                    return "nsfm";
                    break;
                case POST_PROPERTY_PI:
                    return "pi";
                    break;
                case POST_PROPERTY_MODIFIED:
                    return "modified";
                    break;
                default:
                    break;
            }
        }

        public function save_post()
        {
            if(!$this->user_ref->logged_in())
            {
                $this->last_error_msg = "User not logged in.";
                return false;
            }
            if(!$this->user_ref->property(USER_RIGHT_SAVE_POST))
            {
                $this->last_error_msg = "User does not have the right to save a post.";
                return false;
            }
            if($this->is_new && !utility::is_uuid($this->post_uuid))
            {
                $this->post_uuid = utility::generate_uuid();
            }
            if($this->is_new && !utility::is_uuid($this->owner_uuid))
            {
                $this->owner_uuid = $this->user_ref->user_uuid();
            }
            if(!$this->is_new && ($this->owner_uuid != $this->user_ref->user_uuid()) && (!$this->user_ref->property(USER_RIGHT_ADMIN) && !$this->user_ref->property(USER_RIGHT_MODERATOR)))
            {
                $this->last_error_msg = "User does not have rights to edit this post.";
                return false;
            }
            if($this->DB->query($this->construct_save_data_query()))
            {
                $this->is_new = false;
                $this->load_failed = false;
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public function db_error_message()
        {
            return $this->DB->error_code()." ".$this->DB->error_str();
        }
        public function error_message()
        {
            return $this->last_error_msg;
        }
        
        private function generate_vote_query($bool = null)
        {
            if($bool === true)
            {
                //upvote
                $vote_val = POST_VOTE_VAL_UPVOTED;
            }
            else
            {
                // downvote
                $vote_val = POST_VOTE_VAL_DOWNVOTED;                
            }
            return "INSERT INTO ".POST_DB_TABLE_POST_VOTES." (".VOTE_DB_COL_POST_UUID.", ".VOTE_DB_COL_USER_UUID.", ".VOTE_DB_COL_VOTE.", ".VOTE_DB_COL_TIMESTAMP.") VALUES(CONV_TXT_BIN_UUID('".$this->post_uuid."'), CONV_TXT_BIN_UUID('".$this->user_ref->user_uuid()."'), ".$vote_val.", UNIX_TIMESTAMP())";
        }

        public function upvote()
        {
            if(!$this->user_ref->logged_in())
            {
                $this->last_error_msg = "User not logged in.";
                return false;
            }
            if($this->userupvoted)
            {
                $this->last_error_msg = "Already voted.";
                return false;
            }
            if(!$this->remove_user_votes())
            {
                $this->last_error_msg = "Error clearing votes.";
                return false;                
            }
            $query = $this->generate_vote_query((bool)true);
            if($this->DB->query($query))
            {
                return true;
            }
            else
            {
                $this->last_error_msg = "Error upvoting post. ".$query;
                return false;
            }
        }
        
        public function downvote()
        {
            if(!$this->user_ref->logged_in())
            {
                $this->last_error_msg = "User not logged in.";
                return false;
            }
            if($this->user_downvoted)
            {
                $this->last_error_msg = "Already voted.";
                return false;
            }
            if(!$this->remove_user_votes())
            {
                $this->last_error_msg = "Error clearing votes.";
                return false;                
            }
            $query = $this->generate_vote_query((bool)false);
            if($this->DB->query($query))
            {
                return true;
            }
            else
            {
                $this->last_error_msg = "Error downvoting post.";
                return false;
            }            
        }
        
        private function load_user_votes()
        {
            $query = "SELECT ".VOTE_DB_COL_VOTE." FROM ".POST_DB_TABLE_POST_VOTES." WHERE ".VOTE_DB_COL_POST_UUID." = CONV_TXT_BIN_UUID('".$this->post_uuid."') AND ".VOTE_DB_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$this->user_ref->user_uuid()."')";
            if($this->DB->query($query))
            {
                $resarr = $this->DB->result->fetch_assoc();
                $num_res = $this->DB->affected_rows();
                $this->DB->result->close();
                if($num_res > 0)
                {
                    if($resarr['vote'] == POST_VOTE_VAL_UPVOTED)
                    {
                        $this->userupvoted = true;
                        $this->user_downvoted = false;
                    }
                    if($resarr['vote'] == POST_VOTE_VAL_DOWNVOTED)
                    {
                        $this->user_downvoted = true;
                        $this->userupvoted = false;
                    }
                }
                else
                {
                    $this->user_downvoted = false;
                    $this->userupvoted = false;                    
                }
                return true;
            }
            else
            {
                $this->last_error_msg = "Loading post votes failed.";
                return false;
            }
        }
        
        public function load_post_votes()
        {
            $query = "SELECT (SELECT COUNT(*) FROM ".POST_DB_TABLE_POST_VOTES." WHERE ".VOTE_DB_COL_VOTE." = ".POST_VOTE_VAL_DOWNVOTED." AND ".VOTE_DB_COL_POST_UUID." = CONV_TXT_BIN_UUID('".$this->post_uuid."')) as downvotes, (SELECT COUNT(*) FROM ".POST_DB_TABLE_POST_VOTES." WHERE ".VOTE_DB_COL_VOTE." = ".POST_VOTE_VAL_UPVOTED." AND ".VOTE_DB_COL_POST_UUID." = CONV_TXT_BIN_UUID('".$this->post_uuid."')) as upvotes";
            if($this->DB->query($query))
            {
                $resarr = $this->DB->result->fetch_assoc();
                $this->DB->result->close();
                $this->upvotes = $resarr['upvotes'];
                $this->downvotes = $resarr['downvotes'];
                return true;
            }
            else
            {
                $this->last_error_msg = "Loading post votes failed. ".$query;
                return false;
            }
        }
        
        public function remove_user_votes()
        {
            $query = "DELETE FROM ".POST_DB_TABLE_POST_VOTES." WHERE ".VOTE_DB_COL_POST_UUID." = CONV_TXT_BIN_UUID('".$this->post_uuid."') AND ".VOTE_DB_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$this->user_ref->user_uuid()."')";
            if($this->DB->query($query))
            {
                return true;
            }
            else
            {
                $this->last_error_msg = "Clearing votes failed.";
                return false;
            }
        }
        
        public function upvotes()
        {
            return $this->upvotes;
        }
        
        public function downvotes()
        {
            return $this->downvotes;
        }

        public function user_has_upvoted()
        {
            return $this->userupvoted;
        }
        
        public function user_has_downvoted()
        {
            return $this->user_downvoted;
        }
        
        public static function generate_post_selecting_query($mode, $offset, $number_of_posts, $timestamp)
        {
            if(!utility::could_be_int($number_of_posts))
            {
                $number_of_posts = 10;
            }
            if(!utility::could_be_int($offset))
            {
                $offset = 0;
            }
            if(!utility::could_be_int($timestamp))
            {
                $timestamp = time();
            }
            switch($mode)
            {
            case 'top':
                return   "SELECT"
                        ."  CONV_BIN_TXT_UUID(b.".POST_DB_COL_POST_UUID.") as ".POST_DB_COL_POST_UUID.", "
                        ."  SUM(CASE WHEN a.vote = ".POST_VOTE_VAL_UPVOTED." THEN 1 ELSE 0 END)-SUM(CASE WHEN a.vote = ".POST_VOTE_VAL_DOWNVOTED." THEN 1 ELSE 0 END) as ranking "
                        ."FROM "
                        ."  ".POST_DB_TABLE_POSTS." b "
                        ."  LEFT JOIN ".POST_DB_TABLE_POST_VOTES." a ON a.".VOTE_DB_COL_POST_UUID." = b.".POST_DB_COL_POST_UUID." "
                        ."WHERE a.".VOTE_DB_COL_TIMESTAMP." < $timestamp AND substr(b.".POST_DB_COL_PROPERTIES.", ".((POST_PROPERTY_LIVE)+1) .", 1) = 1 "
                        ."GROUP BY b.".POST_DB_COL_POST_UUID." "
                        ."ORDER BY ranking desc "
                        ."LIMIT ".$offset.", ".$number_of_posts;
                break;
            case 'reltop':
                return   "SELECT"
                        ."  CONV_BIN_TXT_UUID(b.".POST_DB_COL_POST_UUID.") as ".POST_DB_COL_POST_UUID.", "
                        ."  SUM(CASE WHEN a.vote = ".POST_VOTE_VAL_UPVOTED." THEN 1 ELSE 0 END)/($timestamp-b.created)*10000-SUM(CASE WHEN a.vote = ".POST_VOTE_VAL_DOWNVOTED." THEN 1 ELSE 0 END)/($timestamp-b.created)*10000 as ranking "
                        ."FROM "
                        ."  ".POST_DB_TABLE_POSTS." b "
                        ."  LEFT JOIN ".POST_DB_TABLE_POST_VOTES." a ON a.".VOTE_DB_COL_POST_UUID." = b.".POST_DB_COL_POST_UUID." "
                        ."WHERE a.".VOTE_DB_COL_TIMESTAMP." < $timestamp AND substr(b.".POST_DB_COL_PROPERTIES.", ".((POST_PROPERTY_LIVE)+1) .", 1) = 1 "
                        ."GROUP BY b.".POST_DB_COL_POST_UUID." "
                        ."ORDER BY ranking desc "
                        ."LIMIT ".$offset.", ".$number_of_posts;
                break;
            case 'time':
                return   "SELECT"
                        ."  CONV_BIN_TXT_UUID(b.".POST_DB_COL_POST_UUID.") as ".POST_DB_COL_POST_UUID." "
                        ."FROM "
                        ."  ".POST_DB_TABLE_POSTS." b "
                        ."  WHERE ".POST_DB_COL_CREATED." <= $timestamp AND substr(b.".POST_DB_COL_PROPERTIES.", ".((POST_PROPERTY_LIVE)+1) .", 1) = 1 "
                        ."ORDER BY created desc "
                        ."LIMIT ".$offset.", ".$number_of_posts;
                break;
            default:
                return false;
                break;
            }
        }

        public function __destruct()
        {
            
        }
    }
?>