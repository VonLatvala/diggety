<?php
    require_once 'inc'.DIRECTORY_SEPARATOR.'env_vars.php';
    require_once 'inc'.DIRECTORY_SEPARATOR.'recaptcha'.DIRECTORY_SEPARATOR.'recaptchalib.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'db.class.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'utility.class.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'post.class.php';
    require_once "inc/FB_API/base_facebook.php";
    require_once "inc/FB_API/facebook.php";
    /**
     * Created  13.11.2012 20:54 GMT+2<br />
     * Modified 13.11.2012 20:54 GMT+2, Axel Latvala
     * 
     * System is the class which holds all other classes.
     *
     * @author  Axel Latvala
     * @version 1.0
     */
    class System
    {
        private $utility_ref;
        private $user_ref;
        private $db_ref;
        
        private $captcha_priv = CAPTCHA_PRIV;
        private $captcha_pub = CAPTCHA_PUB;


        const ERR_INVALID_INPUT = 400;
        const ERR_SERVER_ERROR = 500;
        
        public function __construct()
        {
            session_start();
            $this->utility_ref = new utility;
            $this->db_ref = new DB(ENV_MAIN_MYSQL_HOST, ENV_MAIN_MYSQL_USER, ENV_MAIN_MYSQL_PASSWORD, ENV_MAIN_MYSQL_DB_NAME);
            if(!isset($_SESSION['user_uuid']))
            {
                $_SESSION['user_uuid'] = NULL;
            }
            $this->user_ref = new User($_SESSION['user_uuid'], $this->db_ref);
        }
        
        public function fb_login()
        {
            if($this->user_ref->facebook_authorize())
            {
                $raw['success'] = true;
                $raw['uid'] = $_SESSION['user_uuid'];
            }
            else
            {
                $raw['success'] = false;
                $raw['errormsg'] = $this->user_ref->error_message();
            }
            echo json_encode($raw);
        }

        public function user_info()
        {
            return $this->user_ref->to_array();
        }

        public function run_api()
        {
            if(isset($_POST['q']))
            {
                $mode = 'POST';
                $raw['POST_q'] = $_POST['q'];
            }
            else if(isset($_GET['q']))
            {
                $mode = 'GET';
                $raw['GET_q'] = $_GET['q'];
            }
            else
            {
                $mode = 'ERROR';
            }
            $raw['mode'] = $mode;
            if($mode == 'POST')
            {
                switch($_POST['q'])
                {
                    // POST request
                    case 'nop':
                        $raw['success'] = true;
                        $raw['res'] = 'ZZZzzZZZz...';
                        $raw['errormsg'] = 'OK';
                        echo json_encode($raw);
                        break;
                    case 'fb_login':
                        $facebook = new Facebook();
                        if($facebook->getUser() != 0)
                        {
                            if(!$this->user_ref->facebook_id_login($facebook->getUser()))
                            {
                                $raw['errormsg'] = $this->user_ref->error_message();
                                $raw['success'] = false;
                                echo json_encode($raw);
                                return false;
                            }
                            $raw['success'] = true;
                            $raw['status'] = 1;
                        }
                        break;
                    case 'login':
                        $not_set = array();
                        if(!isset($_POST['user']))
                        {
                            $not_set[] = "User";
                        }
                        if(!isset($_POST['password']))
                        {
                            $not_set[] = "Password";
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $user = new User(NULL, $this->db_ref);
                        if($user->do_login($_POST['user'], $_POST['password']))
                        {
                            if(isset($_POST['remember_me']))
                            {
                                if($_POST['remember_me'] === 'true')
                                {
                                    setcookie('diggety_user', $_POST['user'], (int)(time()+60*60*24*365*10), '/', $_SERVER['HTTP_HOST'], false, false);

                                    setcookie('diggety_pass', $_POST['password'], (int)(time()+60*60*24*365*10), '/', $_SERVER['HTTP_HOST'], false, false);
                                }
                                else
                                {
                                    setcookie('diggety_user', '', time()+60, '/', $_SERVER['HTTP_HOST'], false, false);
                                    setcookie('diggety_pass', '', time()+60, '/', $_SERVER['HTTP_HOST'], false, false);
                                }
                            }
                            $raw['success'] = true;
                            $raw['username'] = $user->username();
                            echo json_encode($raw);
                            return true;
                        }
                        else
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $user->error_message();
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            echo json_encode($raw);
                            return false;
                        }
                        break;
                    case 'new_post':
                        $not_set = array();
                        $post = new Post($this->db_ref, NULL, $this->user_ref);
                        if(!isset($_POST['title']))
                        {
                            $not_set[] = 'Title';
                        }
                        if(!isset($_POST['link']))
                        {
                            $not_set[] = 'Link';
                        }
                        if(!isset($_POST['live']))
                        {
                            $not_set[] = 'Live';
                        }
                        if(!isset($_POST['live']))
                        {
                            $not_set[] = 'Live';
                        }
                        if(!isset($_POST['nsfw']))
                        {
                            $not_set[] = 'NSFW';
                        }
                        if(!isset($_POST['pi']))
                        {
                            $not_set[] = 'PI';
                        }
                        if(!isset($_POST['nsfm']))
                        {
                            $not_set[] = 'NSFM';
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $errornous_field = array();
                        $field_error = array();
                        
                        if(!$post->title($_POST['title']))
                        {
                            $errornous_field[] = 'Title';
                            $field_error[] = $post->error_message();
                        }
                        if(!$post->link($_POST['link']))
                        {
                            $errornous_field[] = 'Link';
                            $field_error[] = $post->error_message();
                        }
                        if(!$post->property(POST_PROPERTY_LIVE, (bool)$_POST['live']))
                        {
                            $errornous_field[] = 'Live';
                            $field_error[] = $post->error_message();
                        }
                        if(!$post->property(POST_PROPERTY_NSFW, (bool)$_POST['nsfw']))
                        {
                            $errornous_field[] = 'NSFW';
                            $field_error[] = $post->error_message();
                        }
                        if(!$post->property(POST_PROPERTY_PI, (bool)$_POST['pi']))
                        {
                            $errornous_field[] = 'PI';
                            $field_error[] = $post->error_message();
                        }
                        if(!$post->property(POST_PROPERTY_NSFM, (bool)$_POST['nsfm']))
                        {
                            $errornous_field[] = 'NSFM';
                            $field_error[] = $post->error_message();
                        }
                        if(count($errornous_field) > 0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Invalid data.";
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errornous_field'] = $errornous_field;
                            $raw['field_error'] = $field_error;
                            echo json_encode($raw);
                            return false;
                        }
                        elseif(!$post->save_post())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return false;
                        }
                        if(!$post->upvote())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return false;
                        }
                        else
                        {
                            $raw['res'] = array();
                            $raw['res']['post_uuid'] = $post->post_uuid();
                            $raw['success'] = true;
                            echo json_encode($raw);
                            return false;
                        }
                        echo json_encode($raw);
                        break;
                    case 'upvote':
                        if(!isset($_POST['post_uuid']))
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Post UUID not specified!";
                            echo json_encode($raw);
                            return false;
                        }
                        $post = new Post($this->db_ref, $_POST['post_uuid'], $this->user_ref);
                        if($post->load_failed())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return false;
                        }
                        if($post->upvote())
                        {
                            $raw['success'] = true;
                            $post->load_post_votes();
                            $raw['upvotes'] = $post->upvotes();
                            $raw['downvotes'] = $post->downvotes();
                            echo json_encode($raw);
                            return true;
                        }
                        else
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return true;
                        }
                        break;
                    case 'downvote':
                        if(!isset($_POST['post_uuid']))
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Post UUID not specified!";
                            echo json_encode($raw);
                            return false;
                        }
                        $post = new Post($this->db_ref, $_POST['post_uuid'], $this->user_ref);
                        if($post->load_failed())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return false;
                        }
                        if($post->downvote())
                        {
                            $raw['success'] = true;
                            $post->load_post_votes();
                            $raw['upvotes'] = $post->upvotes();
                            $raw['downvotes'] = $post->downvotes();
                            echo json_encode($raw);
                            return true;
                        }
                        else
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return true;
                        }
                        break;
                    case 'remove_vote':
                        if(!isset($_POST['post_uuid']))
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Post UUID not specified!";
                            echo json_encode($raw);
                            return false;
                        }
                        $post = new Post($this->db_ref, $_POST['post_uuid'], $this->user_ref);
                        if($post->load_failed())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return false;
                        }
                        if($post->remove_user_votes())
                        {
                            $raw['success'] = true;
                            $post->load_post_votes();
                            $raw['upvotes'] = $post->upvotes();
                            $raw['downvotes'] = $post->downvotes();
                            echo json_encode($raw);
                            return true;
                        }
                        else
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return true;
                        }
                        break;
                    case 'post_votes':
                        if(!isset($_POST['post_uuid']))
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Post UUID not specified!";
                            echo json_encode($raw);
                            return false;
                        }
                        $post = new Post($this->db_ref, $_POST['post_uuid'], $this->user_ref);
                        if($post->load_failed())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $post->error_message();
                            echo json_encode($raw);
                            return false;
                        }
                        $raw['success'] = true;
                        $raw['upvotes'] = $post->upvotes();
                        $raw['downvotes'] = $post->downvotes();
                        $raw['user_upvoted'] = $post->user_has_upvoted();
                        $raw['user_downvoted'] = $post->user_has_downvoted();
                        echo json_encode($raw);
                        return true;
                        break;
                    case 'toggle_nsfw':
                        $not_set = array();
                        $post = new Post($this->db_ref, NULL, $this->user_ref);
                        if(!isset($_POST['post_uuid']))
                        {
                            $not_set[] = 'Post UUID';
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        if($post->load_data($_POST['post_uuid']))
                        {
                            $post->property(POST_PROPERTY_NSFW, ($post->property(POST_PROPERTY_NSFW)=='1'?false:true));
                            if($post->save_post())
                            {
                                $raw['success'] = true;
                                echo json_encode($raw);
                                return true;
                            }
                            else
                            {
                                $raw['success'] = false;
                                if($this->user_ref->property(USER_RIGHT_DEBUG_USER))
                                {
                                    $raw['errormsg'] = "The server barfed.";
                                }
                                else
                                {
                                    $raw['errormsg'] = "The server barfed.";
                                    $raw['db_error'] = $post->db_error_message();
                                }
                                echo json_encode($raw);
                                return false;
                            }
                        }
                        else
                        {
                            $raw['success'] = false;
                            if($this->user_ref->property(USER_RIGHT_DEBUG_USER))
                            {
                                $raw['errormsg'] = "The server barfed.";
                            }
                            else
                            {
                                $raw['errormsg'] = $post->error_message();
                                $raw['db_error'] = $post->db_error_message();
                            }
                            echo json_encode($raw);
                            return false;
                        }
                        break;
                    case 'toggle_nsfm':
                        $not_set = array();
                        $post = new Post($this->db_ref, NULL, $this->user_ref);
                        if(!isset($_POST['post_uuid']))
                        {
                            $not_set[] = 'Post UUID';
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        if($post->load_data($_POST['post_uuid']))
                        {
                            $post->property(POST_PROPERTY_NSFM, ($post->property(POST_PROPERTY_NSFM)=='1'?false:true));
                            if($post->save_post())
                            {
                                $raw['success'] = true;
                                echo json_encode($raw);
                                return true;
                            }
                            else
                            {
                                $raw['success'] = false;
                                if($this->user_ref->property(USER_RIGHT_DEBUG_USER))
                                {
                                    $raw['errormsg'] = "The server barfed.";
                                }
                                else
                                {
                                    $raw['errormsg'] = "The server barfed.";
                                    $raw['db_error'] = $post->db_error_message();
                                }
                                echo json_encode($raw);
                                return false;
                            }
                        }
                        else
                        {
                            $raw['success'] = false;
                            if($this->user_ref->property(USER_RIGHT_DEBUG_USER))
                            {
                                $raw['errormsg'] = "The server barfed.";
                            }
                            else
                            {
                                $raw['errormsg'] = $post->error_message();
                                $raw['db_error'] = $post->db_error_message();
                            }
                            echo json_encode($raw);
                            return false;
                        }
                        break;
                    case 'toggle_pi':
                        $not_set = array();
                        $post = new Post($this->db_ref, NULL, $this->user_ref);
                        if(!isset($_POST['post_uuid']))
                        {
                            $not_set[] = 'Post UUID';
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        if($post->load_data($_POST['post_uuid']))
                        {
                            $post->property(POST_PROPERTY_PI, ($post->property(POST_PROPERTY_PI)=='1'?false:true));
                            if($post->save_post())
                            {
                                $raw['success'] = true;
                                echo json_encode($raw);
                                return true;
                            }
                            else
                            {
                                $raw['success'] = false;
                                if($this->user_ref->property(USER_RIGHT_DEBUG_USER))
                                {
                                    $raw['errormsg'] = "The server barfed.";
                                }
                                else
                                {
                                    $raw['errormsg'] = "The server barfed.";
                                    $raw['db_error'] = $post->db_error_message();
                                }
                                echo json_encode($raw);
                                return false;
                            }
                        }
                        else
                        {
                            $raw['success'] = false;
                            if($this->user_ref->property(USER_RIGHT_DEBUG_USER))
                            {
                                $raw['errormsg'] = "The server barfed.";
                            }
                            else
                            {
                                $raw['errormsg'] = $post->error_message();
                                $raw['db_error'] = $post->db_error_message();
                            }
                            echo json_encode($raw);
                            return false;
                        }
                        break;
                    case 'flag_post':
                        $not_set = array();
                        $post = new Post($this->db_ref, NULL, $this->user_ref);
                        if(!isset($_POST['post_uuid']))
                        {
                            $not_set[] = 'Post UUID';
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }                        
                        break;
                    case 'register':
                        $not_set = array();
                        if(!isset($_POST['username']))
                        {
                            $not_set[] = "Username";
                        }
                        if(!isset($_POST['password']))
                        {
                            $not_set[] = "Password";
                        }
                        if(!isset($_POST['email']))
                        {
                            $not_set[] = "eMail";
                        }
                        if(!isset($_POST['first_name']))
                        {
                            $not_set[] = "First name";
                        }
                        if(!isset($_POST['last_name']))
                        {
                            $not_set[] = "Last name";
                        }
                        if(!isset($_POST['birthdate']))
                        {
                            $not_set[] = "Birth date";
                        }
                        if(!isset($_POST['country_code']))
                        {
                            $not_set[] = "Country";
                        }
                        if(!isset($_POST['gender']))
                        {
                            $not_set[] = "Gender";
                        }
                        if(!isset($_POST['captcha_challenge']))
                        {
                            $not_set[] = "Captcha Challenge";
                        }
                        if(!isset($_POST['captcha_response']))
                        {
                            $not_set[] = "Captcha Response";
                        }                        
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $errornous_field = array();
                        $field_error = array();
                        $new_user = new User(NULL, $this->db_ref);
                        if(empty($_POST['username']))
                        {
                            $errornous_field[] = "Username";
                            $field_error[] = "Username is empty.";
                        }
                        elseif(strlen($_POST['username'])<3)
                        {
                            $errornous_field[] = "Username";
                            $field_error[] = "Username is too short. (Min. 3 char)";
                        }
                        elseif($this->user_ref->user_exists_username($_POST['username']) == array(true, true))
                        {
                            $errornous_field[] = "Username";
                            $field_error[] = "Username taken.";
                        }
                        elseif(!$new_user->username($_POST['username']))
                        {
                            $errornous_field[] = "Username";
                            $field_error[] = $new_user->error_message();
                        }
                        if(empty($_POST['password']))
                        {
                            $errornous_field[] = "Password";
                            $field_error[] = "Password is empty";                            
                        }
                        elseif(strlen($_POST['password'])<6)
                        {
                            $errornous_field[] = "Password";
                            $field_error[] = "Password is too short. (Min. 6 chars)";
                        }
                        if(empty($_POST['email']))
                        {
                            $errornous_field[] = "Email";
                            $field_error[] = "eMail is empty.";
                        }
                        elseif($this->user_ref->user_exists_email($_POST['email']) == array(true, true))
                        {
                            $errornous_field[] = "Email";
                            $field_error[] = "eMail is already registered.";                            
                        }
                        elseif(!$new_user->email($_POST['email']))
                        {
                            $errornous_field[] = "Email";
                            $field_error[] = $new_user->error_message();                            
                        }
                        if(empty($_POST['first_name']))
                        {
                            // Not required
                        }
                        elseif(!$new_user->first_name($_POST['first_name']))
                        {
                            $errornous_field[] = "Firstname";
                            $field_error[] = $new_user->error_message();
                        }
                        if(empty($_POST['last_name']))
                        {
                            // Not required
                        }
                        elseif(!$new_user->last_name($_POST['last_name']))
                        {
                            $errornous_field[] = "Lastname";
                            $field_error[] = $new_user->error_message();
                        }
                        if(empty($_POST['birthdate']))
                        {
                            $errornous_field[] = "Birthdate";
                            $field_error[] = 'Date of birth is empty.';
                        }
                        elseif(!$new_user->birthdate(strtotime($_POST['birthdate'])))
                        {
                            $errornous_field[] = "Birthdate";
                            $field_error[] = $new_user->error_message();
                        }
                        if($_POST['country_code'] == "-" || empty($_POST['country_code']))
                        {
                            // Not required
                        }
                        elseif(!$new_user->country_code($_POST['country_code']))
                        {
                            $errornous_field[] = "Country";
                            $field_error[] = $new_user->error_message();
                        }
                        if($_POST['gender'] == '-' || empty($_POST['gender']))
                        {
                            // Not required
                        }
                        elseif(!($_POST['gender'] == USER_PROPERTY_VAL_GENDER_FEMALE || $_POST['gender'] == USER_PROPERTY_VAL_GENDER_MALE))
                        {
                            $errornous_field[] = "Gender";
                            $field_error[] = "Invalid gender data.";
                        }
                        else
                        {
                            if(!($new_user->property(USER_PROPERTY_GENDER, (bool)$_POST['gender'])))
                            {
                                $errornous_field[] = "Gender";
                                $field_error[] = "Error setting gender.";
                            }
                        }
                        $resp = $resp = recaptcha_check_answer($this->captcha_priv,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["captcha_challenge"],
                                $_POST["captcha_response"]);
                        if(!$resp->is_valid)
                        {
                            $errornous_field[] = "Captcha";
                            $field_error[] = "You entered the captcha wrong.";
                        }
                        if(count($errornous_field) > 0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Invalid data.";
                            $raw['errorcode'] = self::ERR_INVALID_INPUT;
                            $raw['errornous_field'] = $errornous_field;
                            $raw['field_error'] = $field_error;
                            echo json_encode($raw);
                            return false;
                        }
                        else
                        {
                            $new_user->property(USER_RIGHT_COMMENT, true);
                            $new_user->property(USER_RIGHT_SAVE_POST, true);
                            if($new_user->save_data())
                            {
                                if($new_user->set_password($_POST['password']))
                                {
                                    $raw['success'] = true;
                                    $raw['user_uuid'] = $new_user->user_uuid();
                                    echo json_encode($raw);
                                    return true;
                                }
                                else
                                {
                                    $raw['success'] = false;
                                    $raw['errormsg'] = $new_user->error_message();
                                    $raw['errorcode'] = self::ERR_SERVER_ERROR;
                                    echo json_encode($raw);
                                    return false;
                                }
                            }
                            else
                            {
                                $raw['success'] = false;
                                $raw['errormsg'] = $new_user->error_message();
                                $raw['errorcode'] = self::ERR_SERVER_ERROR;
                                echo json_encode($raw);
                                return false;
                            }
                        }
                        break;
                    case 'stress_test':
                        if(!$this->user_ref->logged_in())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "User is not logged in.";
                            echo json_encode($raw);
                            return false;
                        }
                        if(!$this->user_ref->property(USER_RIGHT_DEBUG_USER))
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "User does not have the required rights.";
                            echo json_encode($raw);
                            return false;
                        }
                        // First, add 100 users.
                        $userarr = array();
                        for($i=0;$i<100;$i++)
                        {
                            $user = new User(null, $this->db_ref);
                            if($user->email(utility::random_string(15).'@example.com') && $user->username(substr("TU".sha1(microtime().rand(0,10000)), 0, USER_STRLEN_USERNAME)) && $user->first_name("Anon".utility::random_string(6)) && $user->last_name('Anonson'.utility::random_string(10)) && $user->birthdate(time()-rand((60*60*24*7*4*12*18), (60*60*24*7*4*12*30))) && $user->country_code(utility::random_string(3)) && $user->property(USER_RIGHT_SAVE_POST, true) && $user->property(USER_PROPERTY_IS_DUMMY_USER, true) && $user->save_data())
                            {
                                $userarr[$i] = $user->user_uuid();
                            }
                            else
                            {
                                $raw['success'] = false;
                                $raw['errormsg'] = $user->error_message();
                                echo json_encode($raw);
                                return false;
                            }
                        }
                        
                        // each user posts 10 links
                        $created_posts = array();
                        foreach($userarr as $key => $value)
                        {
                            $curuser = new User($value, $this->db_ref);
                            for($i=0;$i<=10;$i++)
                            {
                                $newpost = new Post($this->db_ref, NULL, $curuser);
                                if($newpost->title(utility::random_string(20)) && $newpost->link("http://i.imgur.com/".utility::random_string(7).".png") && $newpost->property(POST_PROPERTY_LIVE, true) && $newpost->save_post())
                                {
                                    $created_posts[] = $newpost->post_uuid();
                                }
                                else
                                {
                                    $raw['success'] = false;
                                    $raw['errormsg'] = $newpost->error_message();
                                    echo json_encode($raw);
                                    return false;
                                }
                            }
                        }
                        
                        // Each user likes 50 random posts.
                        $upvotes = array();
                        foreach($userarr as $value)
                        {
                            $curuser = new User($value, $this->db_ref);
                            $upvotes[$curuser->user_uuid()] = array();
                            for($i=1;$i<=50;$i++)
                            {
                                $thispost = new Post($this->db_ref, $created_posts[rand(0, count($created_posts)-1)], $curuser);
                                if($thispost->upvote())
                                {
                                    $upvotes[$curuser->user_uuid()][] = $thispost->post_uuid();
                                }
                                else
                                {
                                    $i--;
                                }
                            }
                        }
                        // Each user dislikes 200 random posts.
                        $downvotes = array();
                        foreach($userarr as $value)
                        {
                            $curuser = new User($value, $this->db_ref);
                            $downvotes[$curuser->user_uuid()] = array();
                            for($i=1;$i<=50;$i++)
                            {
                                $thispost = new Post($this->db_ref, $created_posts[rand(0, count($created_posts)-1)], $curuser);
                                if($thispost->downvote())
                                {
                                    $downvotes[$curuser->user_uuid()][] = $thispost->post_uuid();
                                }
                                else
                                {
                                    $i--;
                                }
                            }
                        }
                        $raw['userarr'] = $userarr;
                        $raw['created_posts'] = $created_posts;
                        $raw['upvotes'] = $upvotes;
                        $raw['downvotes'] = $downvotes;
                        $raw['success'] = true;
                        $raw['users'] = $userarr;
                        echo json_encode($raw);
                        return true;
                        break;
                    case 'get_posts':
                        $not_set = array();
                        if(!isset($_POST['offset']))
                        {
                            $not_set[] = "offset";
                        }
                        if(!isset($_POST['num_of_posts']))
                        {
                            $not_set[] = "num_of_posts";
                        }
                        if(!isset($_POST['mode']))
                        {
                            $not_set[] = "mode";
                        }
                        if(!isset($_POST['timestamp']))
                        {
                            $_POST['timestamp'] = time();
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $errornous_field = array();
                        $field_error = array();
                        if(!utility::could_be_int($_POST['offset']))
                        {
                            $errornous_field[] = "offset";
                            $field_error[] = "Not a valid integer.";
                        }
                        if(!utility::could_be_int($_POST['num_of_posts']))
                        {
                            $errornous_field[] = "num_of_posts";
                            $field_error[] = "Not a valid integer.";
                        }
                        if(!in_array($_POST['mode'], array('top', 'reltop', 'time')))
                        {
                            $errornous_field[] = "mode";
                            $field_error[] = "Unknown mode.";
                        }
                        if(!utility::could_be_int($_POST['timestamp']))
                        {
                            $errornous_field[] = "timestamp";
                            $field_error[] = "Not a valid integer.";
                        }
                        if(count($errornous_field)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Errornous fields: ";
                            $raw['error_fields'] = $errornous_field;
                            $raw['field_error'] = $field_error;
                            foreach($errornous_field as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $query = Post::generate_post_selecting_query($_POST['mode'], $_POST['offset'], $_POST['num_of_posts'], $_POST['timestamp']);
                        if(!$query)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Error generating query.";
                            echo json_encode($raw);
                            return false;
                        }
                        if($this->user_ref->property(USER_RIGHT_DEBUG_USER))
                        {
                            $raw['query'] = $query;
                        }
                        if(!$this->db_ref->query($query))
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Error selecting posts.";
                            if($this->user_ref->property(USER_RIGHT_DEBUG_USER))
                            {
                                $raw['technical_error'] = $this->db_error();
                            }
                            echo json_encode($raw);
                            return false;
                        }
                        $posts = array();
                        $post_uuids = array();
                        while($res = $this->db_ref->result->fetch_assoc())
                        {
                            $post_uuids[] = $res['post_uuid'];
                        }
                        for($i=0;$i<=count($post_uuids)-1;$i++)
                        {
                            $post_num = count($posts);
                            $post = new Post($this->db_ref, $post_uuids[$i], $this->user_ref);
                            if($post->load_failed())
                            {
                                $posts[$post_num]['success'] = false;
                            }
                            elseif(!$post->property(POST_PROPERTY_LIVE))
                            {
                                
                            }
                            else
                            {
                                $posts[$post_num]['success'] = true;
                                $posts[$post_num]['URL'] = $post->link();
                                $posts[$post_num]['downvotes'] = $post->downvotes();
                                $posts[$post_num]['upvotes'] = $post->upvotes();
                                $post_owner = new User($post->owner_uuid(), $this->db_ref);
                                $posts[$post_num]['owner_username'] = $post_owner->username();
                                $posts[$post_num]['owner_uuid'] = $post_owner->user_uuid();
                                $posts[$post_num]['title'] = $post->title();
                                $posts[$post_num]['post_uuid'] = $post->post_uuid();
                                $posts[$post_num]['user_upvoted'] = $post->user_has_upvoted();
                                $posts[$post_num]['user_downvoted'] = $post->user_has_downvoted();
                                $posts[$post_num]['created'] = $post->created();
                                $posts[$post_num]['edited']  = $post->edited();
                                $posts[$post_num]['nsfw'] = $post->property(POST_PROPERTY_NSFW);
                                $posts[$post_num]['nsfm'] = $post->property(POST_PROPERTY_NSFM);
                                $posts[$post_num]['pi'] = $post->property(POST_PROPERTY_PI);
                            }
                            unset($post_owner);
                        }
                        $raw['num_posts'] = count($posts);
                        $raw['posts'] = $posts;
                        $raw['success'] = true;
                        echo json_encode($raw);
                        unset($posts);
                        return true;
                        break;
                    case 'post_details':
                    case 'get_post':
                        $not_set = array();
                        if(!isset($_POST['post_uuid']))
                        {
                            $not_set[] = "post_uuid";
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $errornous_field = array();
                        $field_error = array();
                        if(!utility::is_uuid($_POST['post_uuid']))
                        {
                            $errornous_field[] = "post_uuid";
                            $field_error[] = "Not a valid uuid.";
                        }
                        if(count($errornous_field)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Errornous fields: ";
                            $raw['error_fields'] = $errornous_field;
                            $raw['field_error'] = $field_error;
                            foreach($errornous_field as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $post = new Post($this->db_ref, $_POST['post_uuid'], $this->user_ref);
                        if($post->load_failed())
                        {
                            $raw['success'] = false;
                        }
                        elseif(!$post->property(POST_PROPERTY_LIVE))
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Post has been removed.";
                        }
                        else
                        {
                            $raw['success'] = true;
                            $res['created'] = $post->created();
                            $res['edited']  = $post->edited();
                            $res['URL'] = $post->link();
                            $res['downvotes'] = $post->downvotes();
                            $res['upvotes'] = $post->upvotes();
                            $post_owner = new User($post->owner_uuid(), $this->db_ref);
                            $res['owner_username'] = $post_owner->username();
                            $res['owner_uuid'] = $post_owner->user_uuid();
                            $res['title'] = $post->title();
                            $res['post_uuid'] = $post->post_uuid();
                            $res['user_upvoted'] = $post->user_has_upvoted();
                            $res['user_downvoted'] = $post->user_has_downvoted();
                            $raw['res'] = $res;
                            unset($post_owner);
                        }
                        echo json_encode($raw);
                        return true;
                        break;
                    case 'url_info':
                        $not_set = array();
                        if(!isset($_POST['url']))
                        {
                            $not_set[] = "url";
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $errornous_field = array();
                        if(!parse_url($_POST['url']))
                        {
                            $errornous_field[] = "url";
                        }
                        if(count($errornous_field)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Errornous fields: ";
                            $raw['error_fields'] = $errornous_field;
                            foreach($errornous_field as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $raw['res'] = utility::url_info($_POST['url']);
                        $raw['success'] = true;
                        echo json_encode($raw);
                        break;
                    case 'env_info':
                        $raw['success'] = true;
                        $raw['res'] = $this->user_ref->to_array();
                        $raw['res']['servertime'] = time();
                        echo json_encode($raw);
                        break;
                    case 'remove_post':
                        $not_set = array();
                        if(!isset($_POST['post_uuid']))
                        {
                            $not_set[] = "post_uuid";
                        }
                        if(count($not_set)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Missing fields: ";
                            $raw['error_fields'] = array();
                            foreach($not_set as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        if(!$this->user_ref->logged_in())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "User not logged in.";
                            echo json_encode($raw);
                            return false;
                        }
                        $errornous_field = array();
                        $field_error = array();
                        if(!utility::is_uuid($_POST['post_uuid']))
                        {
                            $errornous_field[] = "post_uuid";
                            $field_error[] = "Not a valid uuid.";
                        }
                        if(count($errornous_field)>0)
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Errornous fields: ";
                            $raw['error_fields'] = $errornous_field;
                            $raw['field_error'] = $field_error;
                            foreach($errornous_field as $value)
                            {
                                $raw['error_fields'][] = $value;
                                $raw['errormsg'] .= $value . ", ";
                            }
                            $raw['errormsg'] = trim($raw['errormsg'], ", ");
                            echo json_encode($raw);
                            return false;
                        }
                        $post = new Post($this->db_ref, $_POST['post_uuid'], $this->user_ref);
                        if($post->is_new())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Post not found.";
                            echo json_encode($raw);
                            return false;
                        }
                        if(!$post->property(POST_PROPERTY_LIVE, false))
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Error removing post.".$post->error_message();
                            echo json_encode($raw);
                            return false;                            
                        }
                        if(!$post->save_post())
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Error removing post.".$post->error_message();
                            echo json_encode($raw);
                            return false;                            
                        }
                        $raw['success'] = true;
                        echo json_encode($raw);
                        return true;
                        break;
                    default:
                        $raw['success'] = false;
                        $raw['errormsg'] = 'Unknown command';
                        echo json_encode($raw);
                        break;
                }
            }
            else if($mode == 'GET')
            {
                // GET Request
                switch($_GET['q'])
                {
                    case 'nop':
                        $raw['success'] = true;
                        $raw['errormsg'] = 'OK';
                        echo json_encode($raw);
                        break;
                    case 'fb_login':
                        $this->user_ref->facebook_authorize();
                        break;
                    case 'logout':
                        $this->user_ref->log_out();
                        if(!isset($_SESSION['user_uuid']))
                        {
                            $raw['success'] = true;
                        }
                        else
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = $this->user_ref->error_message();
                        }
                        echo json_encode($raw);
                        break;
                    case 'user_info':
                        $raw['res'] = null;
                        if($this->user_ref->logged_in())
                        {
                            $raw['res'] = $this->user_ref->to_array();
                        }
                        else
                        {
                            $raw['errormsg'] = "User not logged in.";
                        }
                        if(is_array($raw['res']))
                        {
                            $raw['success'] = true;
                        }
                        else
                        {
                            $raw['success'] = false;
                        }
                        echo json_encode($raw);
                        break;
                    case 'property':
                        if(isset($_GET['uuid']))
                            $post_uuid = $_GET['uuid'];
                        else
                            $post_uuid = false;

                        if(isset($_GET['property']))
                            $property = $_GET['property'];
                        else
                            $property = false;

                        if(isset($_GET['val']))
                            $value = $_GET['val'];
                        else
                            $value = -1;

                        if($post_uuid)
                        {
                            $post = new Post($this->db_ref, $post_uuid);

                            if($post->load_failed() === false)
                            {
                                $res = $post->property($property, $value);
                                if($res !== true && $res !== false)
                                {
                                    $raw['res'] = $res;
                                    $raw['success'] = true;
                                }
                                else
                                {
                                    $raw['success'] = $res;
                                }
                            }
                            else
                            {
                                $raw['success'] = false;
                                $raw['errormsg'] = "Error loading post.";
                            }
                            unset($post);
                        }
                        else
                        {
                            $raw['success'] = false;
                            $raw['errormsg'] = "Please specify post UUID.";
                        }
                        echo json_encode($raw);
                        break;
                    default:
                        $raw['success'] = false;
                        $raw['errormsg'] = 'Unknown command';
                        echo json_encode($raw);
                        break;
                }
            }
            else
            {
                $raw['success'] = false;
                $raw['errormsg'] = 'No command specified.';
                echo json_encode($raw);
            }
        }
        
        private function db_error()
        {
            return "MySQL Error(".$this->db_ref->error_code()."): ".$this->db_ref->error_str();
        }
    }

?>
