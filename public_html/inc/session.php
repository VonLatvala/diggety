<?php
    /*
     * File:
     *  session.php
     * Summary:
     *  Handles all session related stuff
     *  Ex. Are we logged in?
     */
    
    // Begin/Resume session
    session_start();
    
?>