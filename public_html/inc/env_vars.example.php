<?php
    /**
     * Env vars for LATVALA_SERVER
     * 
     * @author Axel Latvala
     * @version 1.0
     * Created  08.11.2012 23:58 GMT+2
     */
    define('ENV_MAIN_MYSQL_HOST', 'db.example.com');
    define('ENV_MAIN_MYSQL_USER', 'mysql_user');
    define('ENV_MAIN_MYSQL_PASSWORD', 'secret_password');
    define('ENV_MAIN_MYSQL_DB_NAME', 'diggety_main');
    // eg. /var/www/diggety
    define('HOME_DIR', DIRECTORY_SEPARATOR.'path'.DIRECTORY_SEPARATOR.'to'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'html');
    define('INC_DIR', HOME_DIR.DIRECTORY_SEPARATOR.'inc');
    define('CLASS_DIR', INC_DIR.DIRECTORY_SEPARATOR.'classes');
    
    define('USE_CDN', false);
    if(USE_CDN)
        define('CDN_PATH', 'http://s3.amazonaws.com/diggety');
    else
        define('CDN_PATH', '');
    
    define('CAPTCHA_PRIV', "0000000000FFFFFFFFFF0000000000FFFFFFFFFF");
    define('CAPTCHA_PUB', "FFFFFFFFF0000000000FFFFFFFFF0000000000");