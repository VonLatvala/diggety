$(
    function()
    {
        window.diggety = {};
        window.diggety.env = {};
        refresh_env_info();
        setInterval(refresh_env_info, 1000*60*15);
        $('#login_button').bind('click', prompt_login);
        $('#register_button').bind('click', prompt_register);
        $('#submit_button').bind('click', prompt_submit);
        $('#profile_button').bind('click', prompt_user_management)
    }
)

function set_cookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

(function(){
    var cookies;

    function readCookie(name,c,C,i){

        c = document.cookie.split('; ');
        cookies = {};

        for(i=c.length-1; i>=0; i--){
           C = c[i].split('=');
           cookies[C[0]] = C[1];
        }

        return cookies[name];
    }

    window.readCookie = readCookie; // or expose it however you want
})();

function refresh_load_post_timestamp()
{
    window.diggety.load_top_timestamp = Math.round(((new Date()).getTime()+window.diggety.TimeAdjustment)/1000);
}

function refresh_env_info()
{
    var dataObj = {};
    dataObj.q = 'env_info';
    
    var eCB = function()
    {
        
    }
    var sCB = function(data)
    {
        if(!data)
        {
            eCB();
            return false;
        }
        if(!data.res)
        {
            eCB();
        }
        window.diggety.env = data.res;
    }
    api_POST(dataObj, sCB, eCB);
}

function load_top(offset, num_of_posts, sCB, eCB)
{
    console.log('Loading posts with mode '+diggety.top_type);
    show_loading_posts();
    var requestData = {};
    requestData.q = 'get_posts';
    requestData.offset = offset;
    requestData.num_of_posts = num_of_posts;
    requestData.timestamp = window.diggety.load_top_timestamp;
    requestData.mode = window.diggety.top_type;
    var successCallback = function(data, textStatus, jqXHR)
    {
        hide_loading_post();
        if(data.success == true)
        {
            if(data.num_posts>0 && $('tr.post').size()<1)
            {
                $('tr.announce').remove();
            }
            for(var i=0;i<=data.num_posts-1;i++)
            {
                var obj = data.posts[i];
                var post = generate_post(obj);
                $('#posts tbody').append(post);
                $('tr[uuid="'+obj.post_uuid+'"]').hide();
                $('tr[uuid="'+obj.post_uuid+'"]').show({effect:'fade', easing:'easeOutExpo', duration:800})
            }
            if(data.num_posts<1)
            {
                window.diggety.reached_last_post = true;
                $('#posts tbody').append(generate_no_new_posts());
            }
        }
        if(typeof(sCB) == 'function')
        {
            sCB();
        }
    }
    var errorCallback = function(jqXHR, textStatus, errorThrown)
    {
        hide_loading_post();
        if(typeof(eCB) == 'function')
        {
            eCB();
        }
    }
    api_POST(requestData, successCallback, errorCallback);
}

function generate_post(obj)
{
    if(obj.hasOwnProperty('post_uuid') && obj.hasOwnProperty('title') && obj.hasOwnProperty('URL') && obj.hasOwnProperty('upvotes') && obj.hasOwnProperty('downvotes') && obj.hasOwnProperty('owner_uuid') && obj.hasOwnProperty('owner_username') && obj.hasOwnProperty('user_downvoted') && obj.hasOwnProperty('user_upvoted') && obj.hasOwnProperty('owner_username') && obj.hasOwnProperty('owner_uuid') && obj.hasOwnProperty('nsfw') && obj.hasOwnProperty('nsfm') && obj.hasOwnProperty('pi'))
    {
        var post_uuid = obj.post_uuid;
        var title = obj.title;
        var URL = obj.URL;
        var upvotes = obj.upvotes;
        var downvotes = obj.downvotes;
        var user_downvoted = obj.user_downvoted;
        var user_upvoted = obj.user_upvoted;
        var owner_username = obj.owner_username;
        var owner_uuid = obj.owner_uuid;
        var created = obj.created;
        var edited = obj.edited;
        var is_edited = created == edited ? false : true;
        var is_nsfw = obj.nsfw;
        var is_nsfm = obj.nsfm;
        var is_pi = obj.pi;
    }
    else
    {
        return false;
    }
    var res = "";
    res += '<tr class="post" uuid="'+post_uuid+'">';
        res += '<td class="postopt">';
            res += '<div class="options">';
                res += '<div class="postvotes">';
                    res += '<div class="votedistribution">';
                        res += '<div class="upvotes">';
                            res += upvotes;
                        res += '</div>';
                        res += '<div class="upvote'+(user_upvoted?' upvoted':'')+'"></div>';
                        res += '<div class="downvote'+(user_downvoted?' downvoted':'')+'"></div>';
                        res += '<div class="downvotes">';
                            res += downvotes;
                        res += '</div>';
                    res += '</div>';
                res += '</div>';
                res += '<div class="post_badges">';
                    if(is_nsfw == true)
                    {
                        res += '<img src="images/nsfw_badge_75.png" title="Not safe for Work" alt="Not safe for Work"/>';
                    }
                    if(is_nsfm == true)
                    {
                        res += '<img src="images/nsfm_badge_75.png" title="Not safe for Morals" alt="Not safe for Morals"/>';
                    }
                    if(is_pi == true)
                    {
                        res += '<img src="images/pi_badge_75.png" title="Politically Incorrect" alt="Politically Incorrect"/>';
                    }
                res += '</div>';
            res += '</div>';
        res += '</td>';
    res += '<td class="postcont">';
    res += '<div class="postcontent">';
    var showpreview = false;
    if(URI(URL).protocol() == "")
    {
        URL = '//'+URL;
    }
    var filename = URI(URL).filename().split(".");
    var ext = filename[filename.length-1];
    var has_ext = (filename.length>1?true:false);
    var previewtype = false;
    var vidprovider = false;
    
    if(URI(URL).domain() == 'youtube.com' && URI(URL).filename() == 'watch')
    {
        vidprovider = 'youtube';
        if(typeof(URI.parseQuery(URI(URL).query()).v) != 'undefined')
        {
            var vid_id = URI.parseQuery(URI(URL).query()).v;
            showpreview = true;
            previewtype = 'vid';
        }
    }
    
    if(URI(URL).domain() == "imgur.com" && URI(URL).subdomain() == "i")
    {
        if(!has_ext)
        {
            URL += ".png";
            ext = "png";
        }
    }
    
    var allowedimgs = ["jpg", "jpeg", "png", "gif", "bmp"];
    if($.inArray(ext, allowedimgs) != -1)
    {
        previewtype = 'img';
        showpreview = true;
    }
    res += '<div class="thumbnail_container">';
    res += '<a href="javascript:void(0)" class="thumbnail_link">';
    res += '<span class="thumbresizer">';
    if(showpreview && previewtype == 'vid' && vidprovider == 'youtube')
    {
        res += '<img class="thumbnail_img" src="//img.youtube.com/vi/'+vid_id+'/mqdefault.jpg" />';
    }
    else if(showpreview && previewtype == 'img')
    {
        res += '<img class="thumbnail_img" src="'+URL+'" />';
    }
    else
    {
        res += '<img class="thumbnail_img" src="/images/no_preview_100.png" />';
    }
    res += '</span>';
    res += '</a>';
    res += '</div>';
    res += '<div class="postinfo">';
    if(showpreview == true)
    {
        res += '<a class="post_expand" href="javascript:void(0)" post_uuid="'+post_uuid+'">'+title+' </a>';
    }
    if(showpreview == false)
    {
        res += '<a class="post_expand" href="'+URL+'" post_uuid="'+post_uuid+'" target="_blank">'+title+' </a>';
    }
    res += '<span class="poster">Posted by: <a class="poster_user" href="javascript:void(0)" user_uuid="'+owner_uuid+'">'+owner_username+'</a>';
    res += '<br />';
    res += '<div class="post_controls">';
    res += 'Posted '+render_PHP_timestamp(created)+' | ';
    if(window.diggety.env.logged_in)
    {
        if(window.diggety.env.is_mod || window.diggety.env.is_admin || owner_uuid == window.diggety.env.user_uuid)
        {
            res += ' <a href="javascript:void(0)" class="remove_link" post_uuid="'+post_uuid+'">Remove</a> | ';
        }
    }
    res += ' <a href="'+URL+'" target=\"_blank\">source</a>';
    res += '</div>';
    res += '</div>';
    if(showpreview && previewtype == 'img')
    {
        res += '<div class="imgcont toggleme" style="display:none;">';
        res += '<a href="'+URL+'" target="_new">';
        res += '<img src="'+URL+'" />';
        res += '</a>';
        res += '</div>';
    }
    else if(showpreview && previewtype == 'vid')
    {
        if(vidprovider == 'youtube')
        {
            res += '<div class="vidcont toggleme" style="display:none;">';
            res += '<iframe src="//www.youtube.com/embed/'+vid_id+'?wmode=transparent" frameborder="0" allowfullscreen></iframe>';
            res += '</a>';
            res += '</div>';
        }
    }
    res += '</div>';
    res += '</td>';
    res += '</tr>';
    return res;
}

function generate_no_new_posts()
{
    var res = "";
    res += '<tr class="announce">';
        res += '<td colspan="2">';
            res += 'Seems like you have seen all the posts available right now.';
        res += '</td>';
    res += '</tr>';
    return res;
}

function render_PHP_timestamp(unix_time)
{
    var js_time = unix_time*1000;
    var dateObj = new Date(js_time);
    var sec = ('00'+dateObj.getSeconds()).slice(-2);
    var min = ('00'+dateObj.getMinutes()).slice(-2);
    var hour = ('00'+dateObj.getHours()).slice(-2);
    var day = ('00'+dateObj.getDate()).slice(-2);
    var month = ('00'+(dateObj.getMonth()+1)).slice(-2);
    var year = ('0000'+dateObj.getFullYear()).slice(-4);
    
    return day+'.'+month+'.'+year+' '+hour+':'+min+':'+sec;
}

function generate_wait_dialog()
{
    var res = '';
    res += '<div id="waitdialog">';
    res += '</div>';
    return res;
}

function update_post(obj)
{
    if(!obj.hasOwnProperty('uuid') && !obj.hasOwnProperty('hPost'))
    {
        return false;
    }
    
    if(obj.hasOwnProperty('hPost'))
    {
        var post = obj.hPost;
    }
    else if(obj.hasOwnProperty('uuid'))
    {
        var post = $('tr[uuid="'+obj.uuid+'"]');
    }
    
    if(post.length == 1)
    {
        if(obj.hasOwnProperty('upvotes'))
        {
            $('div.upvotes', post).html(obj.upvotes);
        }
        if(obj.hasOwnProperty('downvotes'))
        {
            $('div.downvotes', post).html(obj.downvotes);
        }
        if(obj.hasOwnProperty('URL'))
        {
            $('div.smallcontent a', post).attr("href", obj.URL);
        }
        if(obj.hasOwnProperty('title'))
        {
            $('div.smallcontent a', post).html(obj.title);
        }
        if(obj.hasOwnProperty('user_downvoted'))
        {
            if($('.downvote', post).hasClass('downvoted'))
            {
                // Post is downvoted.
                if(!obj.user_downvoted)
                {
                    // We are asking to not have downvoted.
                    remove_downvote(post);
                }
            }
            else
            {
                // Post is not downvoted.
                if(obj.user_downvoted)
                {
                    // We are asking to have downvoted.
                    add_downvote(post);
                }
            }
        }
        if(obj.hasOwnProperty('user_upvoted'))
        {
            if($('.upvote', post).hasClass('upvoted'))
            {
                // Post is upvoted.
                if(!obj.user_upvoted)
                {
                    // We are asking to not have upvoted.
                    remove_upvote(post)
                }
            }
            else
            {
                // Post is not upvoted.
                if(obj.user_upvoted)
                {
                    // We are asking to have upvoted.
                    add_upvote(post)
                }
            }            
        }
        return true;
    }
    else
    {
        return false;
    }
    return false;
}

function remove_upvote(hPost)
{
    $('.upvote', hPost).removeClass('upvoted');
}

function add_upvote(hPost)
{
    $('.upvote', hPost).addClass('upvoted');
}

function remove_downvote(hPost)
{
    $('.downvote', hPost).removeClass('downvoted');
}

function add_downvote(hPost)
{
    $('.downvote', hPost).addClass('downvoted');
}

function api_GET(dataObj, successCallback, errorCallback)
{
    $.ajax
    ({
        url:'api.php',
        data:dataObj,
        type:'GET',
        success:successCallback,
        error:errorCallback
    })
}

function api_POST(dataObj, successCallback, errorCallback)
{
    $.ajax
    ({
        url:'api.php',
        data:dataObj,
        type:'POST',
        success:successCallback,
        error:errorCallback
    })
}

function vote_post(hPost, vote)
{
    var isUpvoted = false;
    var isDownvoted = false;
    if(!hPost.length || !(hPost.attr('uuid') != false))
    {
        return false;
    }
    if($('div.upvote', hPost).hasClass('upvoted'))
    {
        isUpvoted = true;
        isDownvoted = false;
    }
    if($('div.downvote', hPost).hasClass('downvoted'))
    {
        isDownvoted = true;
        isUpvoted = false;
    }
    var sCB = function(data)
    {
        // Success callback.
        load_and_update_post_votes(hPost);
    }
    var eCB = function()
    {
        update_post({hPost:hPost, user_upvoted:isUpvoted, user_downvoted:isDownvoted});
    }
    if(vote === 1)
    {
        if(isDownvoted)
        {
            // Remove downvoted status from arrow.
            remove_downvote(hPost);
        }
        
        if(isUpvoted)
        {
            // Remove vote
            remove_upvote(hPost);
            var dataObj = 
            {
                q:"remove_vote",
                post_uuid:hPost.attr('uuid')
            }
            remove_upvote(hPost);
            api_POST(dataObj, sCB, eCB);
        }
        else
        {
            // Upvote
            add_upvote(hPost);
            var dataObj = 
            {
                q:"upvote",
                post_uuid:hPost.attr('uuid')
            }
            api_POST(dataObj, sCB, eCB);
        }
    }
    else if(vote === -1)
    {
        if(isUpvoted)
        {
            // Remove upvoted status from arrow.
            remove_upvote(hPost);
        }
        
        if(isDownvoted)
        {
            // Remove vote
            remove_downvote(hPost);
            var dataObj = 
            {
                q:"remove_vote",
                post_uuid:hPost.attr('uuid')
            }
            api_POST(dataObj, sCB, eCB);
        }
        else
        {
            // Downvote
            var dataObj = 
            {
                q:"downvote",
                post_uuid:hPost.attr('uuid')
            }
            add_downvote(hPost);
            api_POST(dataObj, sCB, eCB);
        }
    }
    else
    {
        return false;
    }
}

function load_and_update_post_votes(hPost)
{
    var dataObj = 
    {
        q:"post_votes",
        post_uuid:hPost.attr('uuid')
    }
    var sCB = function(data)
    {
        if(data.success)
        {
            data.hPost = hPost;
            update_post(data);
        }
        else
        {
            console.log('Error updating votes for post '+dataObj.post_uuid);
        }
    }
    var eCB = function()
    {
        console.log('Error updating votes for post '+dataObj.post_uuid);
    }
    api_POST(dataObj, sCB, eCB);
}

function show_loading_posts()
{
    $('#loadingpost').show();
}

function hide_loading_post()
{
    $('#loadingpost').hide();
}

function getDocHeight()
{
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

function generate_login_dialog()
{
    var res = '';
    res += '<div id="login_dialog" class="large_corners" style="display:none;">';
        res += '<div id="login_title">Login</div>';
        res += '<div id="login_subtitle">or Register</div>';
        res += '<img id="login_ajax" src="/images/ajax-loader.gif" style="display:none;" />';
        res += '<div id="login_status" style="display:none;">Please fill in the form and log in!</div>';
        res += '<div id="login_form">';
            res += '<div class="hint">Username or eMail</div>';
            res += '<input type="text" title="Username or eMail" id="login_username" class="logininput ui-corner-all" /><br />';
            res += '<div class="hint">Password</div>';
            res += '<input type="password" title="Password" id="login_password" class="logininput ui-corner-all" /><br />';
            res += '<span id="remember_me_host"><input type="checkbox" id="remember_me" /> Remember me</span>';
            res += '<span id="do_login">DONE</span>';
            res += '<div id="forgot_password_title">Forgot your password? Please enter your email here...</div>';
            res += '<div class="fb-login-button" data-show-faces="true" data-width="250" data-max-rows="1"></div>';
            res += '<input type="text" id="login_forgot_password" class="logininput ui-corner-all" />';
            res += '<span id="do_forgot_password">DONE</span>';
        res += '</div>';
    res += '</div>';
    return res;
}

function generate_user_management_dialog()
{
    var res = '';
    res += '<div id="user_mngmnt_dialog" class="large_corners diggety_dialog" style="display:none;">';
        res += '<div class="dialog_title">User Management</div>';
        res += '<img class="login_ajax" src="/images/ajax-loader.gif" style="display:none;" />';
        res += '<div class="dialog_status" style="display:none;"></div>';
        res += '<div class="dialog_form">';
            res += '<a title="Logout" id="logout_link" href="javascript:$(\'#user_mngmnt_dialog\').dialog(\'close\');logout();">Logout</a><br />';
            res += '<p>More coming soon...</p>';
        res += '</div>';
    res += '</div>';
    return res;
}

function generate_register_dialog(arr)
{
    var res = '';
    res += '<div id="register_dialog" class="large_corners" style="display:none;">';
        res += '<div id="register_title">Register</div>';
        res += '<div id="register_subtitle">or Login</div>';
        res += '<img id="register_ajax" src="/images/ajax-loader.gif" style="display:none;" />';
        res += '<div id="register_status" style="display:none"></div>';
        res += '<div id="register_form">';
            res += '<div class="hint">Username</div>';
            res += '<input type="text" title="Username" id="register_username" class="logininput ui-corner-all" /><br />';
            res += '<div class="hint">eMail</div>';
            res += '<input type="text" title="eMail" id="register_email" class="logininput ui-corner-all" /><br />';
            res += '<div class="hint">Date of birth</div>';
            res += '<input type="text" title="Date of Birth" id="register_date" class="logininput ui-corner-all" /><br />';
            res += '<div class="hint">Password</div>';
            res += '<input type="password" title="Password" id="register_password" class="logininput ui-corner-all" /><br />';
            res += '<div class="hint">Password (again)</div>';
            res += '<input type="password" title="Password (again)" id="register_password_confirm" class="logininput ui-corner-all" /><br />';
            res += '<span id="show_more">More (optional)</span><br />';
            res += '<div id="register_extras" style="display:none;">';
                res += '<div class="hint">First name</div>';
                res += '<input type="text" title="First name" id="register_firstname" class="logininput ui-corner-all" /><br />';
                res += '<div class="hint">Last name</div>';
                res += '<input type="text" title="Last name" id="register_lastname" class="logininput ui-corner-all" /><br />';
                res += '<div class="hint">Gender</div>';
                res += '<select title="Gender" id="register_gender">';
                    res += '<option value="-">Please select...</option>';
                    res += '<option value="1">Male</option>';
                    res += '<option value="0">Female</option>';
                res += '</select>';
                res += '<div class="hint">Country</div>';
                res += '<select title="Country" id="register_country">';
                    res += '<option value="-">Please select...</option>';
                    for(var i=0;i<arr.length;i++)
                    {
                        res += '<option value="'+arr[i].code+'">'+arr[i].name+'</option>'
                    }
                res += '</select>';
            res += '</div>';
            res += '<span id="agree_tos"><input type="checkbox" id="agree_tos_chk" /> I agree to the <a href="/tos.php" target="_blank">Terms of Service</a></span>';
            res += '<span id="do_register">DONE</span>';
        res += '</div>';
        res += '<div id="register_captcha"><div id="captcha_host"></div></div>';
    res += '</div>';
    return res;
}

function generate_submit_dialog()
{
    var res = '';
    res += '<div id="submit_dialog" class="large_corners" style="display:none;">';
        res += '<div id="submit_title">Submit</div>';
        res += '<img id="submit_ajax" src="/images/ajax-loader.gif" style="display:none;" />';
        res += '<div id="submit_status" style="display:none;"></div>';
        res += '<div id="submit_form">';
            res += '<div class="hint">Title</div>';
            res += '<input type="text" title="Title" id="submit_title_val" class="logininput ui-corner-all" /><br />';
            res += '<div class="hint">Link</div>';
            res += '<input type="text" title="URL" id="submit_link" class="logininput ui-corner-all" /><br />';
            res += '<span class="post_prop" title="Not Safe For Work - Anything that could get you into trouble if your boss peeked at your screen."><input type="checkbox" id="NSFW" title="Not Safe For Work - Anything that could get you into trouble if your boss peeked at your screen." /> NSFW</span><br />';
            res += '<span class="post_prop" title="Politically Incorrect - Anything that could offend somebody of a specific culture."><input type="checkbox" id="PI" title="Politically Incorrect - Anything that could offend somebody of a specific culture." /> PI</span>';
            res += '<span id="do_submit">POST</span><br />';
            res += '<span class="post_prop" title="Not Safe For Morals - Anything that could offend somebody who cares a lot about morals (strict people)."><input type="checkbox" id="NSFM" title="Not Safe For Work - Anything that could offend somebody who cares a lot about morals (strict people)." /> NSFM</span>';
        res += '</div>';
    res += '</div>';
    return res;
}

function prompt_login(obj)
{
    if($("#login_dialog").size())
    {
        $("#login_dialog").remove(); 
    }
    $('body').append(generate_login_dialog());
    
    var hMe = $("#login_dialog");
        
    var dialog_options = {};
    dialog_options.title = "Login";
    dialog_options.autoOpen = true;
    
    dialog_options.closeOnEscape = true;
    
    dialog_options.draggable = true;
    
    dialog_options.height = 'auto';
    dialog_options.width = 380;
    
    dialog_options.modal = true;

    dialog_options.resizable = false;
    
    dialog_options.show = 
    {
        effect: "fade",
        duration: 150,
        easing: ""
    };
    
    dialog_options.hide = 
    {
        effect: "fade",
        duration: 300,
        easing: ""
    };
    
    dialog_options.stack = false;
    
    if(!obj)
    {
        obj = {};
    }
    
    if(obj.showEffect)
    {
        dialog_options.show.effect = obj.showEffect
    }
    if(obj.showEasing)
    {
        dialog_options.show.easing = obj.showEasing
    }

    if(obj.hideEffect)
    {
        dialog_options.hide.effect = obj.hideEffect
    }
    if(obj.hideEasing)
    {
        dialog_options.hide.easing = obj.hideEasing
    }
    
    var login = function()
    {
        $('#login_status').hide();
        $('#login_ajax').show();
        
        var errornous_field = [];
        var field_error = [];
        
        if(errornous_field.length>0)
        {
            $('#login_status').html('');
            $.each(field_error, function(key, val)
            {
                $('#login_status').append(val+'<br />');
            })
            $('#login_ajax').hide();
            $('#login_status').show({duration:250, effect:'fade'});
            return false;
        }
        
        var dataObj = {};
        dataObj.q = 'login';
        dataObj.user = $('#login_username', hMe).val();
        dataObj.password = $('#login_password', hMe).val();
        dataObj.remember_me = $('#remember_me', hMe).is(':checked');
        
        var eCB = function()
        {
            $('#login_ajax').hide();
            $('#login_status').html('Something went wrong. Try again.');
            $('#login_status').show({duration:250, effect:'fade'});
        }
        
        var sCB = function(data)
        {
            $('#login_ajax').hide();
            if(!data)
            {
                eCB();
            }
            if(!data.success)
            {
                eCB();
            }
            if(data.success == false)
            {
                $('#login_status').html(data.errormsg);
                $('#login_status').show({duration:250, effect:'fade'});
                return false;
            }

            $('#login_status').html('Successfully logged in!');
            $('#profile_button').text(data.username);
            $('#profile_button').show();
            $('#register_button').hide();
            $('#login_button').hide();
            refresh_env_info();
            hMe.dialog('close');
            refresh_posts();
        }
        
        api_POST(dataObj, sCB, eCB);
    }
    
    dialog_options.open = function()
    {
        $('#login_dialog').parent().css('background-color', 'white');
        $('#login_dialog').parent().css('border', '1px solid #2f4965');
        $('#login_dialog').parent().css('padding', '0');
        $('#login_dialog').parent().addClass('large_corners');
        $('.ui-dialog-titlebar').css('display','none');
        $('.ui-widget-overlay').bind('click', function()
        {
            $('#login_dialog').dialog('close');
        })
        $('.ui-widget-overlay').css('cursor', 'pointer');
    }
    
    $('#login_subtitle').click(function(){hMe.dialog('close');prompt_register();});
    
    $('#do_login').click(function(){login()});
    $('input#login_username').val(readCookie('diggety_user')?readCookie('diggety_user'):'');
    $('input#login_password').val(readCookie('diggety_pass')?readCookie('diggety_pass'):'');
    if(readCookie('diggety_pass') && readCookie('diggety_user'))
    {
        $('#remember_me', hMe).attr('checked', 'checked');
    }
    $('input#login_username, input#login_password', hMe).on('keyup', function(e)
    {
        if(e.which == 13)
        {
            login();
        }
    })
    
    $('#login_dialog').dialog(dialog_options);
}

function prompt_register(obj)
{
    var ISOcodes = "AF:Afghanistan;AX:Åland Islands;AL:Albania;DZ:Algeria;AS:American Samoa;AD:Andorra;AO:Angola;AI:Anguilla;AQ:Antarctica;AG:Antigua and Barbuda;AR:Argentina;AM:Armenia;AW:Aruba;AU:Australia;AT:Austria;AZ:Azerbaijan;BS:Bahamas;BH:Bahrain;BD:Bangladesh;BB:Barbados;BY:Belarus;BE:Belgium;BZ:Belize;BJ:Benin;BM:Bermuda;BT:Bhutan;BO:Bolivia;BA:Bosnia and Herzegovina;BW:Botswana;BV:Bouvet Island;BR:Brazil;IO:British Indian Ocean Territory;BN:Brunei Darussalam;BG:Bulgaria;BF:Burkina Faso;BI:Burundi;KH:Cambodia;CM:Cameroon;CA:Canada;CV:Cape Verde;KY:Cayman Islands;CF:Central African Republic;TD:Chad;CL:Chile;CN:China;CX:Christmas Island;CC:Cocos (Keeling) Islands;CO:Colombia;KM:Comoros;CG:Congo;CD:Congo, The Democratic Republic of The;CK:Cook Islands;CR:Costa Rica;CI:Cote D'ivoire;HR:Croatia;CU:Cuba;CY:Cyprus;CZ:Czech Republic;DK:Denmark;DJ:Djibouti;DM:Dominica;DO:Dominican Republic;EC:Ecuador;EG:Egypt;SV:El Salvador;GQ:Equatorial Guinea;ER:Eritrea;EE:Estonia;ET:Ethiopia;FK:Falkland Islands (Malvinas);FO:Faroe Islands;FJ:Fiji;FI:Finland;FR:France;GF:French Guiana;PF:French Polynesia;TF:French Southern Territories;GA:Gabon;GM:Gambia;GE:Georgia;DE:Germany;GH:Ghana;GI:Gibraltar;GR:Greece;GL:Greenland;GD:Grenada;GP:Guadeloupe;GU:Guam;GT:Guatemala;GG:Guernsey;GN:Guinea;GW:Guinea-bissau;GY:Guyana;HT:Haiti;HM:Heard Island and Mcdonald Islands;VA:Holy See (Vatican City State);HN:Honduras;HK:Hong Kong;HU:Hungary;IS:Iceland;IN:India;ID:Indonesia;IR:Iran, Islamic Republic of;IQ:Iraq;IE:Ireland;IM:Isle of Man;IL:Israel;IT:Italy;JM:Jamaica;JP:Japan;JE:Jersey;JO:Jordan;KZ:Kazakhstan;KE:Kenya;KI:Kiribati;KP:Korea, Democratic People's Republic of;KR:Korea, Republic of;KW:Kuwait;KG:Kyrgyzstan;LA:Lao People's Democratic Republic;LV:Latvia;LB:Lebanon;LS:Lesotho;LR:Liberia;LY:Libyan Arab Jamahiriya;LI:Liechtenstein;LT:Lithuania;LU:Luxembourg;MO:Macao;MK:Macedonia, The Former Yugoslav Republic of;MG:Madagascar;MW:Malawi;MY:Malaysia;MV:Maldives;ML:Mali;MT:Malta;MH:Marshall Islands;MQ:Martinique;MR:Mauritania;MU:Mauritius;YT:Mayotte;MX:Mexico;FM:Micronesia, Federated States of;MD:Moldova, Republic of;MC:Monaco;MN:Mongolia;ME:Montenegro;MS:Montserrat;MA:Morocco;MZ:Mozambique;MM:Myanmar;NA:Namibia;NR:Nauru;NP:Nepal;NL:Netherlands;AN:Netherlands Antilles;NC:New Caledonia;NZ:New Zealand;NI:Nicaragua;NE:Niger;NG:Nigeria;NU:Niue;NF:Norfolk Island;MP:Northern Mariana Islands;NO:Norway;OM:Oman;PK:Pakistan;PW:Palau;PS:Palestinian Territory, Occupied;PA:Panama;PG:Papua New Guinea;PY:Paraguay;PE:Peru;PH:Philippines;PN:Pitcairn;PL:Poland;PT:Portugal;PR:Puerto Rico;QA:Qatar;RE:Reunion;RO:Romania;RU:Russian Federation;RW:Rwanda;SH:Saint Helena;KN:Saint Kitts and Nevis;LC:Saint Lucia;PM:Saint Pierre and Miquelon;VC:Saint Vincent and The Grenadines;WS:Samoa;SM:San Marino;ST:Sao Tome and Principe;SA:Saudi Arabia;SN:Senegal;RS:Serbia;SC:Seychelles;SL:Sierra Leone;SG:Singapore;SK:Slovakia;SI:Slovenia;SB:Solomon Islands;SO:Somalia;ZA:South Africa;GS:South Georgia and The South Sandwich Islands;ES:Spain;LK:Sri Lanka;SD:Sudan;SR:Suriname;SJ:Svalbard and Jan Mayen;SZ:Swaziland;SE:Sweden;CH:Switzerland;SY:Syrian Arab Republic;TW:Taiwan, Province of China;TJ:Tajikistan;TZ:Tanzania, United Republic of;TH:Thailand;TL:Timor-leste;TG:Togo;TK:Tokelau;TO:Tonga;TT:Trinidad and Tobago;TN:Tunisia;TR:Turkey;TM:Turkmenistan;TC:Turks and Caicos Islands;TV:Tuvalu;UG:Uganda;UA:Ukraine;AE:United Arab Emirates;GB:United Kingdom;US:United States;UM:United States Minor Outlying Islands;UY:Uruguay;UZ:Uzbekistan;VU:Vanuatu;VE:Venezuela;VN:Viet Nam;VG:Virgin Islands, British;VI:Virgin Islands, U.S.;WF:Wallis and Futuna;EH:Western Sahara;YE:Yemen;ZM:Zambia;ZW:Zimbabwe";
    
    var country_array = ISOcodes.split(";");
    var processed_array = [];
    $.each(country_array, function(key, val)
    {
        var tmp = val.split(':');
        var code = tmp[0];
        var name = tmp[1];
        processed_array.push({code:code,name:name});
    })
    
    if($("#register_dialog").size())
    {
        $("#register_dialog").remove(); 
    }
    $('body').append(generate_register_dialog(processed_array));
    
    var hMe = $("#register_dialog");
    $('#captcha_host').each(function(){Recaptcha.create('6LdfN9cSAAAAAIybP3yLEKTAwpyoxswRAzxHonKS', this)})

    var dialog_options = {};
    dialog_options.title = "Register";
    dialog_options.autoOpen = true;
    
    dialog_options.closeOnEscape = true;
    
    dialog_options.draggable = true;
    
    dialog_options.height = 'auto';
    dialog_options.width = 380;
    
    dialog_options.modal = true;

    dialog_options.resizable = false;
    
    dialog_options.show = 
    {
        effect: "fade",
        duration: 150,
        easing: ""
    };
    
    dialog_options.hide = 
    {
        effect: "fade",
        duration: 300,
        easing: ""
    };
    
    dialog_options.stack = false;
    
    if(!obj)
    {
        obj = {};
    }
    
    if(obj.showEffect)
    {
        dialog_options.show.effect = obj.showEffect
    }
    if(obj.showEasing)
    {
        dialog_options.show.easing = obj.showEasing
    }

    if(obj.hideEffect)
    {
        dialog_options.hide.effect = obj.hideEffect
    }
    if(obj.hideEasing)
    {
        dialog_options.hide.easing = obj.hideEasing
    }
    
    dialog_options.open = function()
    {
        hMe.parent().css('background-color', 'white');
        hMe.parent().css('border', '1px solid #2f4965');
        hMe.parent().css('padding', '0');
        hMe.parent().addClass('large_corners');
        $('.ui-dialog-titlebar', hMe.parent()).css('display','none');
        $('.ui-widget-overlay').bind('click', function()
        {
            hMe.dialog('close');
        })
        $('.ui-widget-overlay').css('cursor', 'pointer');
    }
    
    $('#register_date', hMe).datepicker({
            dateFormat : 'dd.mm.yy',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            maxDate: '-1d'
        });
    
    var register = function()
    {
        $('#register_status').hide();
        $('#register_ajax').show();
        
        var errornous_field = [];
        var field_error = [];
        
        if($('#register_password', hMe).val() != $('#register_password_confirm', hMe).val())
        {
            errornous_field.push('password');
            field_error.push('Passwords do not match.');
        }
        if(!$('#agree_tos_chk').is(':checked'))
        {
            errornous_field.push('agree_tos_chk');
            field_error.push('You have to agree with the Terms and Conditions.');
        }
        
        if(errornous_field.length>0)
        {
            $('#register_status').html('');
            $.each(field_error, function(key, val)
            {
                $('#register_status').append(val+'<br />');
            })
            $('#register_ajax').hide();
            $('#register_status').show({duration:250, effect:'fade'});
            return false;
        }

        var dataObj = {}
        dataObj.q = 'register';
        dataObj.username = $('#register_username', hMe).val();
        dataObj.password = $('#register_password', hMe).val();
        dataObj.email = $('#register_email', hMe).val();
        dataObj.first_name = $('#register_firstname', hMe).val();
        dataObj.last_name = $('#register_lastname', hMe).val();
        dataObj.country_code = $('#register_country', hMe).val();
        dataObj.gender = $('#register_gender', hMe).val();
        dataObj.birthdate = $('#register_date', hMe).val();
        dataObj.captcha_challenge = $('#captcha_host #recaptcha_challenge_field').val();
        dataObj.captcha_response = $('#captcha_host #recaptcha_response_field').val();
        
        var eCB = function()
        {
            $('#register_ajax').hide();
            $('#register_status').html('Something went wrong.. Please try again.');
            $('#register_status').show({duration:250, effect:'fade'});
        }
        
        var sCB = function(data)
        {
            $('#register_ajax').hide();
            if(!data)
            {
                eCB();
                return false;
            }
            if(!data.hasOwnProperty('success'))
            {
                eCB();
                return false;                
            }
            if(data.success == false)
            {
                if(!data.errormsg)
                {
                    eCB();
                    return false;
                }
                if(data.errormsg = 'Invalid data.')
                {
                    $('#register_status').html('');
                    $.each(data.field_error, function(key, val)
                    {
                        $('#register_status').append(val+'<br />');
                    })
                    $.each(data.errornous_field, function(key, val)
                    {
                        if(val == 'Captcha')
                        {
                            Recaptcha.reload();
                        }
                    })
                    $('#register_status').show({duration:250, effect:'fade'});
                    return false;
                }
                $('#register_status').html(data.errormsg);
                $('#register_status').show({duration:250, effect:'fade'});
                return false;
            }
            $('#register_status').html('Successfully registered! Logging you in...');
            $('#register_status').show({duration:250, effect:'fade'});
            do_login(dataObj.username, dataObj.password);
            setTimeout(function(){hMe.dialog("close")}, 2000);
        }
        api_POST(dataObj, sCB, eCB);
    }
    
    $('#show_more').click(function()
    {
        $('#register_extras').toggle({duration:300,effect:'scale'});
    })
    
    $('#register_subtitle').click(function()
    {
        hMe.dialog('close');
        prompt_login();
    });
    
    $('input', hMe).on('keyup', function(e)
    {
        if(e.which == 13)
        {
            register();
        }
    })
    
    $('#do_register').click(function()
    {
        register();
    })
    
    $('#register_dialog').dialog(dialog_options);
}

function prompt_submit(obj)
{
    if(!window.diggety.env.logged_in)
    {
        prompt_login();
        return false;
    }
    if($("#submit_dialog").size())
    {
        $("#submit_dialog").remove(); 
    }
    $('body').append(generate_submit_dialog());
    
    var hMe = $("#submit_dialog");

    var dialog_options = {};
    dialog_options.title = "Submit";
    dialog_options.autoOpen = true;
    
    dialog_options.closeOnEscape = true;
    
    dialog_options.draggable = true;
    
    dialog_options.height = 'auto';
    dialog_options.width = 380;
    
    dialog_options.modal = true;

    dialog_options.resizable = false;
    
    dialog_options.show = 
    {
        effect: "fade",
        duration: 150,
        easing: ""
    };
    
    dialog_options.hide = 
    {
        effect: "fade",
        duration: 300,
        easing: ""
    };
    
    dialog_options.stack = false;
    
    if(!obj)
    {
        obj = {};
    }
    
    if(obj.showEffect)
    {
        dialog_options.show.effect = obj.showEffect
    }
    if(obj.showEasing)
    {
        dialog_options.show.easing = obj.showEasing
    }

    if(obj.hideEffect)
    {
        dialog_options.hide.effect = obj.hideEffect
    }
    if(obj.hideEasing)
    {
        dialog_options.hide.easing = obj.hideEasing
    }

    var submit = function()
    {
        $('#submit_status', hMe).hide();
        $('#submit_ajax', hMe).show();

        var dataObj = {}
        dataObj.q = 'new_post';
        dataObj.title = $('#submit_title_val', hMe).val();
        dataObj.link = $('#submit_link', hMe).val();
        dataObj.live = 1;
        dataObj.nsfw = $('#NSFW', hMe).is(':checked')?1:0;
        dataObj.pi = $('#PI', hMe).is(':checked')?1:0;
        dataObj.nsfm = $('#NSFM', hMe).is(':checked')?1:0;
        
        var eCB = function()
        {
            $('#submit_ajax', hMe).hide();
            $('#submit_status', hMe).html('Something went wrong.. Please try again.');
            $('#submit_status', hMe).show({duration:250, effect:'fade'});
        }
        
        var sCB = function(data)
        {
            $('#submit_ajax', hMe).hide();
            if(!data)
            {
                eCB();
                return false;
            }
            if(!data.hasOwnProperty('success'))
            {
                eCB();
                return false;                
            }
            if(data.success == false)
            {
                if(!data.errormsg)
                {
                    eCB();
                    return false;
                }
                if(data.errormsg = 'Invalid data.')
                {
                    $('#submit_status', hMe).html('');
                    $.each(data.field_error, function(key, val)
                    {
                        $('#submit_status', hMe).append(val+'<br />');
                    })
                    $.each(data.errornous_field, function(key, val)
                    {
                        if(val == 'Captcha')
                        {
                            Recaptcha.reload();
                        }
                    })
                    $('#submit_status', hMe).show({duration:250, effect:'fade'});
                    return false;
                }
                $('#submit_status', hMe).html(data.errormsg);
                $('#submit_status', hMe).show({duration:250, effect:'fade'});
                return false;
            }
            $('#submit_status', hMe).html('Successfully posted!');
            $('#submit_status', hMe).show({duration:250, effect:'fade'});
            setTimeout(function()
            {
                var dataObj =
                {
                    q:'get_post',
                    post_uuid:data.res.post_uuid
                }
                var sCB = function(data)
                {
                    hMe.dialog("close");
                    if(data.success)
                    {
                        $('tbody').prepend(generate_post(data.res));
                        $('body').scroll(0);
                    }
                    else
                    {

                    }
                }
                var eCB = function()
                {
                    hMe.dialog("close");
                }
                api_POST(dataObj, sCB, eCB);
            }, 1500);
            return true;
        }
        api_POST(dataObj, sCB, eCB);
    }

    dialog_options.open = function()
    {
        hMe.parent().css('background-color', 'white');
        hMe.parent().css('border', '1px solid #2f4965');
        hMe.parent().css('padding', '0');
        hMe.parent().addClass('large_corners');
        $('.ui-dialog-titlebar', hMe.parent()).css('display','none');
        $('.ui-widget-overlay').bind('click', function()
        {
            hMe.dialog('close');
        })
        $('.ui-widget-overlay').css('cursor', 'pointer');
    }
    
    $('#do_submit').click(submit)
    
    $('#submit_dialog').dialog(dialog_options);
}

function prompt_user_management(obj)
{
    if(!window.diggety.env.logged_in)
    {
        prompt_login();
        return false;
    }
    if($("#user_mngmnt_dialog").size())
    {
        $("#user_mngmnt_dialog").remove(); 
    }
    $('body').append(generate_user_management_dialog());
    
    var hMe = $("#user_mngmnt_dialog");

    var dialog_options = {};
    dialog_options.title = "User Management";
    dialog_options.autoOpen = true;
    
    dialog_options.closeOnEscape = true;
    
    dialog_options.draggable = true;
    
    dialog_options.height = 'auto';
    dialog_options.width = 380;
    
    dialog_options.modal = true;

    dialog_options.resizable = false;
    
    dialog_options.show = 
    {
        effect: "fade",
        duration: 150,
        easing: ""
    };
    
    dialog_options.hide = 
    {
        effect: "fade",
        duration: 300,
        easing: ""
    };
    
    dialog_options.stack = false;
    
    if(!obj)
    {
        obj = {};
    }
    
    if(obj.showEffect)
    {
        dialog_options.show.effect = obj.showEffect
    }
    if(obj.showEasing)
    {
        dialog_options.show.easing = obj.showEasing
    }

    if(obj.hideEffect)
    {
        dialog_options.hide.effect = obj.hideEffect
    }
    if(obj.hideEasing)
    {
        dialog_options.hide.easing = obj.hideEasing
    }

    var load_user_data = function()
    {
        
    }

    dialog_options.open = function()
    {
        hMe.parent().css('background-color', 'white');
        hMe.parent().css('border', '1px solid #2f4965');
        hMe.parent().css('padding', '0');
        hMe.parent().addClass('large_corners');
        $('.ui-dialog-titlebar', hMe.parent()).css('display','none');
        $('.ui-widget-overlay').bind('click', function()
        {
            hMe.dialog('close');
        })
        $('.ui-widget-overlay').css('cursor', 'pointer');
        $('#user_mngmnt_dialog a').button();
    }
    
    hMe.dialog(dialog_options);
}

function do_login(username, password)
{
    var dataObj = {};
    dataObj.q = 'login';
    dataObj.user = username;
    dataObj.password = password;
    
    var eCB = function()
    {
        
    }
    
    var sCB = function(data)
    {
        if(!data)
        {
            eCB();
            return false;
        }
        if(!data.hasOwnProperty('success'))
        {
            eCB();
            return false;                
        }
        if(!data.success)
        {
            if(!data.errormsg)
            {
                eCB();
                return false;
            }
        }
        $('#profile_button').text(data.username);
        $('#profile_button').show();
        $('#register_button').hide();
        $('#login_button').hide();
        refresh_env_info();
    }
    api_POST(dataObj, sCB, eCB);
}

function remove_post(post_uuid)
{
    var dataObj = {};
    dataObj.q = "remove_post";
    dataObj.post_uuid = post_uuid;
    
    var eCB = function()
    {
        
    }
    var sCB = function(data)
    {
        if(!data)
        {
            eCB();
            return false;
        }
        if(!data.hasOwnProperty('success'))
        {
            eCB();
            return false;
        }
        if(data.success == false)
        {
            return false;
        }
        $('tr[uuid='+post_uuid+']').hide('slow', function(){$('tr[uuid='+post_uuid+']').remove()});
    }
    
    api_POST(dataObj, sCB, eCB);
}

function logout()
{
    var dataObj = {};
    dataObj.q = "logout";
    
    var eCB = function()
    {
        
    }
    var sCB = function(data)
    {
        if(!data)
        {
            eCB();
            return false;
        }
        if(!data.hasOwnProperty('success'))
        {
            eCB();
            return false;
        }
        if(data.success == false)
        {
            return false;
        }
        $('#profile_button').text('');
        $('#profile_button').hide();
        $('#register_button').show();
        $('#login_button').show();
        refresh_env_info();
        refresh_posts();
    }
    
    api_GET(dataObj, sCB, eCB);
}

function refresh_posts()
{
    console.log('Refreshing posts...');
    if(window.diggety.loading_posts == true)
    {
        return false;
    }
    window.diggety.reached_last_post = false;
    window.diggety.loading_posts = true;
    refresh_load_post_timestamp();
    $('html, body').animate({scrollTop:0}, 'fast');
    $('tr.post').remove();
    window.diggety.listen_to_scroll = false;
    var sCB = function(){window.diggety.listen_to_scroll = true;window.diggety.loading_posts = false;};
    var eCB = function(){window.diggety.listen_to_scroll = true;window.diggety.loading_posts = false;};
    load_top(0, 10, sCB, eCB);
}

makeImageZoomable = function(imageTag)
{
    if (this.options.imageZoom.value) {
        // Add listeners for drag to resize functionality...
        imageTag.addEventListener('mousedown', function(e) {
            if (e.button == 0) {
                if (!imageTag.minWidth) imageTag.minWidth = Math.max(1, Math.min(imageTag.width, 100));
                modules['showImages'].dragTargetData.imageWidth = e.target.width;
                modules['showImages'].dragTargetData.diagonal = modules['showImages'].getDragSize(e);
                modules['showImages'].dragTargetData.dragging = false;
                e.preventDefault();
            }
        }, true);
        imageTag.addEventListener('mousemove', function(e) {
            if (modules['showImages'].dragTargetData.diagonal){
                var newDiagonal = modules['showImages'].getDragSize(e);
                var oldDiagonal = modules['showImages'].dragTargetData.diagonal;
                var imageWidth = modules['showImages'].dragTargetData.imageWidth;
                e.target.style.maxWidth=e.target.style.width=Math.max(e.target.minWidth, newDiagonal/oldDiagonal*imageWidth)+'px';

                e.target.style.maxHeight='';
                e.target.style.height='auto';
                modules['showImages'].handleSidebarHiding(e.target);
                modules['showImages'].dragTargetData.dragging = true;
            }
        }, false);
        imageTag.addEventListener('mouseout', function(e) {
            modules['showImages'].dragTargetData.diagonal = 0;
        }, false);
        imageTag.addEventListener('mouseup', function(e) {
            if (modules['showImages'].dragTargetData.diagonal) {
                var newDiagonal = modules['showImages'].getDragSize(e);
                var oldDiagonal = modules['showImages'].dragTargetData.diagonal;
                var imageWidth = modules['showImages'].dragTargetData.imageWidth;
                e.target.style.maxWidth=e.target.style.width=Math.max(e.target.minWidth, newDiagonal/oldDiagonal*imageWidth)+'px';
            }

            modules['showImages'].handleSidebarHiding();
            modules['showImages'].dragTargetData.diagonal = 0;
        }, false);
        imageTag.addEventListener('click', function(e) {
            modules['showImages'].dragTargetData.diagonal = 0;
            if (modules['showImages'].dragTargetData.dragging) {
                modules['showImages'].dragTargetData.dragging = false;
                e.preventDefault();
                return false;
            }
        }, false);
    }
}