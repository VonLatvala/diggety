<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Diggety - Opens soon!</title>
        <style>
            html, body
            {
                height:100%;
            }
            body
            {
                background:url(tmpimg/Timerbg.png) repeat-x #FFFFFF;
                height:619px;
                margin:0 auto;
            }
            #main
            {
                height:619px;
                width:749px;
                margin:0 auto;
                background:url(tmpimg/main.png);
            }

            .f0
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px 0px;
            }

            .f1
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -26px;
            }

            .f2
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -52px;
            }

            .f3
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -78px;
            }

            .f4
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -104px;
            }

            .f5
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -130px;
            }

            .f6
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -156px;
            }

            .f7
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -182px;
            }

            .f8
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -208px;
            }

            .f9
            {
                height:26px;
                width:19px;
                background:url(tmpimg/fsprite.png) 0px -234px;
            }

            #main div
            {
                display:inline-block;
                position:relative;
                top:589px;
            }

            #w10
            {
                left:43px;
            }

            #w1
            {
                left:39px;
            }

            #d10
            {
                left:138px;
            }

            #d1
            {
                left:134px;
            }

            #h10
            {
                left:211px;
            }

            #h1
            {
                left:207px;
            }

            #min10
            {
                left:294px;
            }

            #min1
            {
                left:290px;
            }

            #s10
            {
                left:407px;
            }

            #s1
            {
                left:403px;
            }
            div#notification
            {
                margin-top: 10px;
                display: block;
                font-size: 30px;
                font-family: arial;
                font-weight: bold;
                text-align: center;
                color:#636161;
            }
            a
            {
                text-decoration: none;
                color:#2e83d3;
            }
            a:hover
            {
                text-decoration: none;
                color:#5b9ad4;
            }
        </style>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript">
            window.diggety = {};
            <?php
                if(isset($_GET['oneminute']))
                {
                    $after1min = strtotime('+ 20sec');
                    $year = date('Y', $after1min);
                    $month = date('m', $after1min);
                    $day = date('j', $after1min);
                    $hour = date('G', $after1min);
                    $minute = date('i', $after1min);
                    $sec = date('s', $after1min);
                    echo "window.diggety.env = InitClock(0,$sec,$minute,$hour,$day,$month,$year);";
                }
                else if(isset($_GET['slowpoke']))
                {
                    $after1min = strtotime('- 1min');
                    $year = date('Y', $after1min);
                    $month = date('m', $after1min);
                    $day = date('j', $after1min);
                    $hour = date('G', $after1min);
                    $minute = date('i', $after1min);
                    $sec = date('s', $after1min);
                    echo "window.diggety.env = InitClock(0,$sec,$minute,$hour,$day,$month,$year);";
                }
                else
                {
                    echo "window.diggety.env = InitClock(0,0,0,23,15,12,2012);";
                }
            ?>

            function InitClock(ms, s, minut, h, day, month, year)
            {
                    var TargetDate = new Date();
                    TargetDate.setMilliseconds(ms);
                    TargetDate.setSeconds(s);
                    TargetDate.setMinutes(minut);
                    TargetDate.setHours(h);
                    TargetDate.setDate(day);
                    TargetDate.setMonth(month-1);
                    TargetDate.setFullYear(year);

                    var ServerTime = new Date(<?php echo time()*1000;?>);

                    var TimeAdjustment = ServerTime - (new Date().getTime());

                    return {'TargetDate':TargetDate, 'TimeAdjustment':TimeAdjustment, 'ServerTime':ServerTime};
            }

            function updateTimer(w10, w1, d10, d1, h10, h1, m10, m1, s10, s1)
            {
                    $('#w10').attr("class", "f"+w10);
                    $('#w1').attr("class", "f"+w1);

                    $('#d10').attr("class", "f"+d10);
                    $('#d1').attr("class", "f"+d1);
 
                    $('#h10').attr("class", "f"+h10);
                    $('#h1').attr("class", "f"+h1);

                    $('#min10').attr("class", "f"+m10);
                    $('#min1').attr("class", "f"+m1);

                    $('#s10').attr("class", "f"+s10);
                    $('#s1').attr("class", "f"+s1);
            }

            function tick()
            {
                    var CurrentTime = (new Date().getTime())+window.diggety.env.TimeAdjustment;
                    var TimeDiff = window.diggety.env.TargetDate.getTime()-CurrentTime;
                    var ms = 1;
                    var sec = 1000*ms;
                    var minute = 60*sec;
                    var hour = 60*minute;
                    var day = 24*hour;
                    var week = 7*day;
                    
                    if(TimeDiff<sec)
                    {
                        $('#notification').html('Hey there! Looks like the website has launched. <a href="index.php">Welcome!</a>');
                        $('#notification').fadeIn(2000);
                        clearInterval(timer);
                        updateTimer
                            (
                                "0",
                                "0",
                                "0",
                                "0",
                                "0",
                                "0",
                                "0",
                                "0",
                                "0",
                                "0"
                            )
                        return;
                    }
                    var leftWeeks = Math.floor(TimeDiff/week);
                    var leftDays = Math.floor((TimeDiff-leftWeeks*week)/day);
                    var leftHours = Math.floor((TimeDiff-leftWeeks*week-leftDays*day)/hour);
                    var leftMinutes = Math.floor((TimeDiff-leftWeeks*week-leftDays*day-leftHours*hour)/minute);
                    var leftSeconds = Math.floor((TimeDiff-leftWeeks*week-leftDays*day-leftHours*hour-leftMinutes*minute)/sec);
                    updateTimer
                        (
                            ("0"+leftWeeks).slice(-2).substr(0,1),
                            ("0"+leftWeeks).slice(-2).substr(1,1),
                            ("0"+leftDays).slice(-2).substr(0,1),
                            ("0"+leftDays).slice(-2).substr(1,1),
                            ("0"+leftHours).slice(-2).substr(0,1),
                            ("0"+leftHours).slice(-2).substr(1,1),
                            ("0"+leftMinutes).slice(-2).substr(0,1),
                            ("0"+leftMinutes).slice(-2).substr(1,1),
                            ("0"+leftSeconds).slice(-2).substr(0,1),
                            ("0"+leftSeconds).slice(-2).substr(1,1)
                        )
            }

            timer = setInterval(tick, 1000);
        </script>
    </head>

    <body>
        <div id="main">
            <div id="w10" class="f0"></div>
            <div id="w1" class="f0"></div>

            <div id="d10" class="f0"></div>
            <div id="d1" class="f0"></div>

            <div id="h10" class="f0"></div>
            <div id="h1" class="f0"></div>

            <div id="min10" class="f0"></div>
            <div id="min1" class="f0"></div>

            <div id="s10" class="f0"></div>
            <div id="s1" class="f0"></div>
            
            <div id="notification" style="display:none;"></div>
            <div id="info" style="display:none;">Diggety is a site for sharing all of your fun or interesting content, and aims to be the easiest and most user friendly website to do so. Welcome on board! Website opens 01.12.2012 12:00 GMT+2</div>
        </div>
    </body>
</html>
<?php
    $link = mysql_connect('localhost', 'diggety', 'vg1PhBtcYWscpWq');
    if(!$link)
    {
        die('<!-- Couldn\'t establish a connection to the statistics databse. -->');
    }
    if(!mysql_select_db('diggety_statistics', $link))
    {
        die('<!-- Couldn\'t select the statistics database. Reason: '. mysql_error($link).' -->');
    }
    $ipv4query = 'INSERT INTO `statistics` (`timestamp`, `IPv4`, `IPv6`, `useragent`, `request_uri`) VALUES (unix_timestamp(), \''.mysql_real_escape_string($_SERVER['REMOTE_ADDR'], $link).'\', \'\', \''.mysql_real_escape_string($_SERVER['HTTP_USER_AGENT'], $link).'\', \''.mysql_real_escape_string($_SERVER['REQUEST_URI'], $link).'\');';
    $ipv6query = 'INSERT INTO `statistics` (`timestamp`, `IPv4`, `IPv6`, `useragent`, `request_uri`) VALUES (unix_timestamp(), \'\', \''.mysql_real_escape_string($_SERVER['REMOTE_ADDR'], $link).'\', \''.mysql_real_escape_string($_SERVER['HTTP_USER_AGENT'], $link).'\', \''.mysql_real_escape_string($_SERVER['REQUEST_URI'], $link).'\');';
    if(strstr($_SERVER['REMOTE_ADDR'], "."))
    {
        $query = $ipv4query;
    }
    else
    {
        $query = $ipv6query;
    }
    if(!mysql_query($query, $link))
    {
        die('<!-- Couldn\'t write the statistics to the statistics database. Reason: '. mysql_error($link).' -->');
    }
    else
    {
        echo "<!-- Statistics successfully updated. -->";
    }
?>