<?php
    ini_set('error_reporting', 1);
    error_reporting(E_ALL);
    require_once 'inc/env_vars.php';
    require_once CLASS_DIR.'/system.class.php';
    $system = new System();
    
    if(isset($_GET['code'])&&isset($_GET['state']))
    {
        if($_SESSION['state'] && ($_SESSION['state'] === $_REQUEST['state']))
        {
            $system->fb_login();
            die();
        }
    }
    session_start();
    if(!isset($_SESSION['user_uuid']) && !isset($_GET['nigga']))
    {
        if(strtotime('15.12.2012 23:00') > time())
        {
            include('countdown.php');
            die();
        }
    }
    $userinfo = $system->user_info();
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" >
    <head>
        <meta charset="utf-8" />
        <title>Diggety - Share your fun!</title>
        <link rel="stylesheet" media="screen" href="<?php echo CDN_PATH; ?>/css/default.css" />
        <link rel="stylesheet" media="screen" href="<?php echo CDN_PATH; ?>/css/jqueryUI/jqueryUI.css" />
    </head>

    <body>
        <div id="fb-root"></div>
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '492479304118920',
              channelUrl : '//<?php echo $_SERVER['HTTP_HOST'] ?>/channel.php',
              status     : true,
              cookie     : true,
              xfbml      : true
            });
          };
          (function(d, debug){
             var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement('script'); js.id = id; js.async = true;
             js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
             ref.parentNode.insertBefore(js, ref);
           }(document, /*debug*/ false));
        </script>
        <div class="ui-corner-all" id="jse" style="display:none;border:2px solid lightgray;position: fixed; left:50%; top:50%; width:500px; height:250px; margin-left: -250px; margin-top: -125px; background: white; z-index: 250;">
            <div style="padding:50px;">
                Welcome to Diggety!<br />
                Diggety is a site for sharing all your fun and interesting content! We are aiming to be a very user friendly and modern place to share links to fun content! Welcome!
            </div>
        </div>
        <div id="headerbar">
            <div id="headercontainer">
                <div id="login_button" class="headerbutton" <?php if($userinfo['logged_in']){echo "style=\"display:none;\"";}?>>Log in</div>
                <div id="register_button" class="headerbutton" <?php if($userinfo['logged_in']){echo "style=\"display:none;\"";}?>>Register</div>
                <div id="profile_button" class="headerbutton" <?php if(!$userinfo['logged_in']){echo "style=\"display:none;\"";}?>><?php echo $userinfo[USER_DB_COL_USERNAME]; ?></div>
                <div id="submit_button" class="headerbutton">Submit</div>
            </div>
        </div>
        <div id="header">
            <span id="logo">
                <!-- Background is logo. From sprite. -->
            </span>
            <div id="links">
                <ul>
                    <li><a id="top_post" href="javascript:void(0)">Highly rated</a></li>
                    <li><a id="trending_post" href="javascript:void(0)">Trending</a></li>
                    <li><a id="new_post" href="javascript:void(0)">New</a></li>
                </ul>
            </div>
        </div>
        <div id="wrapper">
            <div id="content">
                <table id="posts">
                    <tbody>
                        
                    </tbody>
                </table>
                <div id="loadingpost">
                    <p>
                        Loading posts, please wait...<br />
                        <img src="<?php echo CDN_PATH; ?>/images/ajax-loader.gif" alt="Loading..." title="Loading posts, please wait..."/>
                    </p>
                </div>
            </div>
            <div id="side">
                <div class="sidebaritem" style="display: none;">
                    <div class="itemcontainer">
                        
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <script type="text/javascript" src='//www.google.com/recaptcha/api/js/recaptcha_ajax.js'></script>
        <script type="text/javascript" src="<?php echo CDN_PATH; ?>/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo CDN_PATH; ?>/js/jqueryUI.js"></script>
        <script type="text/javascript" src="<?php echo CDN_PATH; ?>/js/URI.js"></script>
        <script type="text/javascript" src="<?php echo CDN_PATH; ?>/js/main.js"></script>
        <script>
            $(function(){
                var ServerTime = new Date(<?php echo time()*1000;?>);
                window.diggety.TimeAdjustment = ServerTime - (new Date().getTime());
                window.diggety.top_type = 'reltop';
                window.diggety.listen_to_scroll = false;
                window.diggety.reached_last_post = false;
                window.diggety.loading_posts = false;
                $(window).scroll(function()
                {
                    if(window.diggety.listen_to_scroll == true && $(window).scrollTop() + $(window).height() > getDocHeight()-100 && window.diggety.loading_posts == false)
                    {
                        window.diggety.loading_posts = true;
                        window.diggety.listen_to_scroll = false;
                        var sCB = function(){if(!window.diggety.reached_last_post){window.diggety.listen_to_scroll = true;}window.diggety.loading_posts = false;};
                        var eCB = function(){if(!window.diggety.reached_last_post){window.diggety.listen_to_scroll = true;}window.diggety.loading_posts = false;};
                        console.log('Loading more posts...');
                        load_top($('tr').length, 10, sCB, eCB);
                    }
                });
                $('body').on('mousedown', '.headerbutton', function() { 
                    if(!$(this).hasClass('header_button_onmousedown'))
                    {
                        $(this).addClass('header_button_onmousedown');
                    }
                });
                $('body').on('mouseup', '.headerbutton', function() { 
                    if($(this).hasClass('header_button_onmousedown'))
                    {
                        $(this).removeClass('header_button_onmousedown');
                    }
                });
                $('body').on('click', '.upvote', function()
                {
                    if(!window.diggety.env.logged_in)
                    {
                        prompt_login();
                        return false;
                    }
                    var hMe = $(this);
                    vote_post($(hMe).parent().parent().parent().parent().parent(), 1)
                });
                $('body').on('click', '.downvote', function()
                {
                    if(!window.diggety.env.logged_in)
                    {
                        prompt_login();
                        return false;
                    }
                    var hMe = $(this);
                    vote_post($(hMe).parent().parent().parent().parent().parent(), -1)
                });

                $('#top_post').click(function()
                {
                    window.diggety.top_type = 'top';
                    window.diggety.listen_to_scroll = true;
                    refresh_posts();
                })
                $('#trending_post').click(function()
                {
                    window.diggety.top_type = 'reltop';
                    window.diggety.listen_to_scroll = true;
                    refresh_posts();
                })
                $('#new_post').click(function()
                {
                    window.diggety.top_type = 'time';
                    window.diggety.listen_to_scroll = true;
                    refresh_posts();
                })
                $('.headerbutton').disableSelection();
                refresh_posts();
                $('body').on('click', 'a.thumbnail_link', function(){$('.toggleme', $(this).parent().parent()).toggle({duration:300, effect:'fade'})})
                $('body').on('click', 'a.post_expand', function(){$('.toggleme', $(this).parent().parent()).toggle({duration:300, effect:'fade'})})
                $('body').on('click', 'a.remove_link', function(){remove_post(($(this).attr('post_uuid')))});
            });
            
        </script>
    </body>
</html>