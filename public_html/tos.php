<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Diggety - Terms of Service</title>
        <style>
            html, body
            {
                font-family: arial;
                width: 600px;
            }
            html, body
            {
                margin:0 auto;
            }
            li
            {
                list-style-type: upper-roman;
            }
        </style>
    </head>
    <body>
        <h1>Terms of Service</h1>


        <h2>RULES</h2>
        <p>
            “Diggety” refers to <?php echo $_SERVER['HTTP_HOST']; ?> and all affiliated domains.<br />
            <strong>NOTICE:</strong> The use of Diggety is a privilege not a right. We reserve the right to delete, modify, or edit content at any time and without notice.
        </p>
        <ul>
            <li>Please do not post, discuss, or link to anything that violates local or Finnish law.</li>

            <li>If it is illegal for you to view any of the content on this site, please discontinue browsing immediately.</li>

            <li>Anything you post that should be categorised as NSFW (Not Safe For Work) or NSFM (Not Safe For Morals) but isn’t, will be deleted.</li>

            <li>Posting of any personal information is forbidden.</li>

            <li>Complaining about Diggety in a post might result in post deletion. If you have any complaints, please contact us directly.</li>

            <li>Spamming and flooding is strictly forbidden, this might lead to a ban of indeterminate length.</li>

            <li>Content posted on Diggety might be inappropriate to people under the age of 14.</li>

            <li>Impersonating as Diggety staff is forbidden.</li>

            <li>Advertising without permission is forbidden and will lead to post deletion and might also lead to a permanent ban.</li>
        </ul>
        <p>
            These rules apply to everyone unless otherwise noted. Breaking any of these rules will lead to a proper punishment which is chosen on a per-case basis, based on the severity. Diggety staff, however, is not obligated to follow these rules.
        </p>


        <strong>USER AGREEMENT</strong> - Revised October 17, 2012

        <p>
            The following User Agreement (“Agreement”) governs the use of <?php echo $_SERVER['HTTP_HOST']; ?> and all affiliated sub domains (“Website”), including without limitation participation on its walls and all other areas of the website as provided by Diggety (“Diggety”, “we”, “us”).
        </p>
        <p>
            YOU CAN ACCESS THE USER AGREEMENT AT ANY TIME AT THE FOLLOWING ADDRESS: <a href="https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];?>"><?php echo "https://". $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];?></a>
            YOUR USE OF THE WEBSITE AND ITS SERVICES CONSTITUTES YOUR AGREEMENT TO COMPLY WITH THE RULES SET FORTH. IF YOU DO NOT AGREE TO THESE RULES FOR ANY REASON, PLEASE DO NOT USE THIS WEBSITE.
        </p>
        <p>
            The User Agreement page may be updated at any time and will appear on this page, so it is important to check back often. Use of the Website and its services constitutes agreement to its terms and conditions. We reserve the right to update the Agreement at any time.
            Failure to comply with the rules set forth may result in suspension or termination of access to the Website without notice.
        </p>

        <h2>Section 1 - Rules of Usage</h2>
        <p>
            We do not knowingly collect personal information from users under the age of 14. Users under the age of 14 are expressly prohibited from submitting personal information and creating accounts on the Website.
        </p>
        <p>
            You may not pretend to be somebody else with using the Website.
        </p>
        <p>
            You agree not to post or write any material that is considered: obscene, indecent, 
            offensive language, defamatory, abusive, bullying, harassing, racist, hateful, or violent.
        </p>
        <p>
            You agree to refrain from writing ethnic slurs, sexual oriented slurs, and personal attacks when using the Website.
        </p>
        <p>
            You may not post or write anything that invades anyone’s privacy, or facilitates or encourages conduct that would constitute a criminal offense, give rise to civil liability, or that otherwise violates any local, state, federal, national or international law or regulation (e.g., drug use, underage drinking). You agree to use the Website only for lawful purposes and you acknowledge that your failure to do so may subject you to civil and criminal liability.
        </p>
        <p>
            You are responsible for ensuring that anything you post or write on the Website does not violate the copyright, trademark, trade secret or any other personal or proprietary rights of any third party or is provided or posted with the permission of the owner(s) of such rights.
        </p>
        <p>
            The Website contains graphics, logos, text, photographs, images, video, audio, software, code, and other material that is provided by Diggety or its licensors and is not clearly identified as, or intended, for your use, including without limitation the organization, design, compilation, and “look and feel” of the Website, and advertising thereon (“Content”). Content is protected by state, national and international copyright, trademark and all other intellectual property laws, and is the property of Diggety or its licensors.
        </p>

        <h2>Section 2  -	Monitoring</h2>
        <p>
            All users agree to the monitoring of the Website. Our goal is to provide a highly enjoyable online experience for all users; to achieve this goal we will have to monitor the activity on the Website, which includes the monitoring of walls.
        </p>

        <h2>Section 3  -	Submissions Not Endorsed by Diggety</h2>
        <p>
            Diggety does not necessarily endorse, or support the comments or expressions posted or written on the website. You agree that Diggety has no liability to you for submissions that others have made to the wall, with respect to any information or materials posted by others, including defamatory, offensive or illicit material, even material that violates this Agreement.
        </p>

        <h2>Section 4  -	User Generated Content</h2>
        <p>
            When you post or otherwise submit content to this website, you give Diggety (and those we work with) a worldwide license to use, host, store, reproduce, modify, create derivative works, communicate, publish, publicly perform, publicly display and distribute such content. In addition, please be aware that information you disclose in publicly accessible portions of the Website will be available to all users of the Website; so be mindful of personal information and other content you may wish to post.
        </p>

        <h2>Section 5 - Termination Or Suspension</h2>
        <p>
            We have the right to terminate and/or suspend your ability to access the Website or any portion thereof, for any or no reason, without notice.
        </p>

        <h2>Section 6  -	Editing, Deleting, or Modifying Content</h2>
        <p>
            We reserve the right at all times, but undertake no duty, to review, edit, or delete any material that is displayed on the Website or its wall.
        </p>

        <h2>Section 7 - Copyright</h2>

        <p>
            This section contains the formal requirements of the Copyright Act with respect to the rights of copyright owners whose content appears on the Website without authorization.
            If you believe that your work have been copied, you may notify service provider by providing the following information (as required by the Online Copyright Infringement Liability Limitation Act of the Digital Millennium Copyright Act, 17 U.S.C. sec. 512) to our copyright agent set forth below:
            To file a copyright infringement notification with the Website, the copyright owner or an agent acting on his or her behalf will need to send a written communication that includes substantially the following:
        </p>
        <ul>
            <li>A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
            <li>Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site.</li>
            <li>Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit the service provider to locate the material. In this regard please provide URLs when you identify the location of the material.</li>
            <li>Information reasonably sufficient to permit the service provider to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted.</li>
            <li>A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law.</li>
            <li>A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
        </ul>
        <p>
            Written notice should be sent to the Diggety’s designated agent as follows:
        </p>
        <p>
            Diggety, Inc.<br />
            attn: Jesse Mervasto<br />
            Bulevardi 24 B10<br />
                Karjaa, Finland<br />
            Email: legal@<?php echo $_SERVER['HTTP_HOST']; ?><br />
        <p>
            Under Section 512(f) of the Copyright Act any person who knowingly materially misrepresents that material or activity is infringing may be subject to liability. Consult your legal counsel or see Section 512(c)(3) of 17 U.S.C. to clarify or confirm the requirements of the notice.
            Only copyright complaints will be answered at the email address and physical address provided above.
        </p>

        <h2>Section 8 - Indemnification</h2>
        <p>
            You agree to indemnify Diggety and its affiliates, employees, agents, representatives and third party service providers, and to defend and hold each of them harmless, from any and all claims and liabilities (including attorneys fees) which may arise from your submissions, from your unauthorized use of material obtained through the Website, or from your breach of this Agreement, or from any such acts through your use of the Website.
        </p>

        <h2>Section 9 - Jurisdiction</h2>
        <p>
            You agree that this Agreement, for all purposes, shall be governed and construed in accordance with the Finnish laws applicable to contracts to be wholly performed therein, and any action based on, relating to, or alleging a breach of this Agreement must be brought in a state or federal court in Finland. In addition, both parties agree to submit to the exclusive personal jurisdiction and venue of such courts.
        </p>
        <h3>Disclaimer Of Warranty And Limitation Of Liability</h3>
        <p>
            YOU ACKNOWLEDGE THAT YOU ARE USING THE WEBSITE AT YOUR OWN RISK. THE WEBSITE IS PROVIDED “AS IS”, AND DIGGETY, ITS AFFILIATES AND ITS THIRD PARTY SERVICE PROVIDERS HEREBY EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES, EXPRESS AND IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF ACCURACY, RELIABILITY, TITLE, MERCHANTABILITY, NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER WARRANTY, CONDITION, GUARANTEE OR REPRESENTATION, WHETHER ORAL, IN WRITING OR IN ELECTRONIC FORM, INCLUDING BUT NOT LIMITED TO THE ACCURACY OR COMPLETENESS OF ANY INFORMATION CONTAINED THEREIN OR PROVIDED BY THE WEBSITE. DIGGETY, ITS AFFILIATES, AND ITS THIRD PARTY SERVICE PROVIDERS DO NOT REPRESENT OR WARRANT THAT ACCESS TO THE WEBSITE WILL BE UNINTERRUPTED OR THAT THERE WILL BE NO FAILURES, ERRORS OR OMISSIONS OR LOSS OF TRANSMITTED INFORMATION, OR THAT NO VIRUSES WILL BE TRANSMITTED ON THE WEBSITE.
            DIGGETY, ITS AFFILIATES AND ITS THIRD PARTY SERVICE PROVIDERS SHALL NOT BE LIABLE TO YOU OR ANY THIRD PARTIES FOR ANY DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES ALLEGEDLY SUSTAINED ARISING OUT OF THIS AGREEMENT, THE PROVIDING OF THE WEBSITE HEREUNDER, THE SALE OR PURCHASE OF ANY GOODS OR MERCHANDISE, YOUR ACCESS TO OR INABILITY TO ACCESS THE WEBSITE, INCLUDING FOR VIRUSES ALLEGED TO HAVE BEEN OBTAINED FROM THE WEBSITE, YOUR USE OF OR RELIANCE ON THE WEBSITE OR ANY OF THE MERCHANDISE, INFORMATION OR MATERIALS AVAILABLE ON THE WEBSITE, REGARDLESS OF THE TYPE OF CLAIM OR THE NATURE OF THE CAUSE OF ACTION, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME COUNTRIES DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE EXCLUSIONS OR LIMITATIONS MAY NOT APPLY TO YOU. YOU MAY ALSO HAVE OTHER RIGHTS THAT VARY FROM COUNTRY TO COUNTRY. YOU HEREBY AGREE NOT TO USE THE WEBSITE WITH THE INTENTION TO DEVELOP, OR OTHERWISE PLAN NUCLEAR BOMBS, OR ENRICH WEAPONS GRADE URANIUM.
            YOU HEREBY AGREE TO RELEASE DIGGETY, ITS AFFILIATES AND THIRD-PARTY SERVICE PROVIDERS, AND EACH OF THEIR RESPECTIVE DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS FROM CLAIMS, DEMANDS AND DAMAGES (ACTUAL AND CONSEQUENTIAL) OF EVERY KIND AND NATURE, KNOWN AND UNKNOWN, SUSPECTED AND UNSUSPECTED, DISCLOSED AND UNDISCLOSED (“CLAIMS”), ARISING OUT OF OR IN ANY WAY CONNECTED WITH YOUR USE OF THIS SITE. IF YOU ARE A CALIFORNIA RESIDENT, YOU WAIVE CALIFORNIA CIVIL CODE SECTION 1542, WHICH STATES, IN PART: “A GENERAL RELEASE DOES NOT EXTEND TO CLAIMS WHICH THE CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN BY HIM MUST HAVE MATERIALLY AFFECTED HIS SETTLEMENT WITH THE DEBTOR”.
        </p>
        <p>
            <strong>NOTICE: If you don’t agree to the terms contained in this Agreement, please leave the Website immediately.</strong>
        </p>
    </body>
</html>