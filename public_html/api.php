<?php
    /*
     * File:
     *  api.php
     * Summary:
     *  Handles the serverside
     * 
     * Modified 12.11.2012 02:04 GMT+2, Axel Latvala
     */

    ini_set('display_errors', 1);
    error_reporting(E_ALL | E_STRICT);
    
    require_once 'inc/env_vars.php';
    require_once CLASS_DIR.'/system.class.php';
    require_once CLASS_DIR.'/db.class.php';
    require_once CLASS_DIR.'/utility.class.php';
    require_once CLASS_DIR.'/post.class.php';
   
    header('Content-type: application/json; charset=utf-8');
    
    $system = new System();
    
    $system->run_api();

?>