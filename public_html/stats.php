<?php
    if(!isset($_GET['salaistatavaraa']))
    {
        header('HTTP/1.0 404 Not Found');
        die('404 NOT FOUND.');
    }
?>
<!DOCYPE HML>
<html>
    <head>
        <title>Diggety - Statistics</title>
        <style>
            body
            {
                font-family: Arial;
            }
            a
            {
                text-decoration: none;
                color:black;
            }
            
        </style>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript">
            $(
            function()
            {
                $('table').on('click', 'a', function()
                {
                    var nodehandle = $(this);
                    var ip = nodehandle.html();
                    var dataObj = {};
                    dataObj.key = '1ef53b03bc2b9ec701a23f3784c0e56fb46c81cf4c0b8fc33bbba649f82b8264';
                    dataObj.ip = ip;
                    dataObj.format = 'json';
                    $.ajax
                    ({
                        crossDomain: true,
                        dataType:'jsonp',
                        type:'get',
                        data:dataObj,
                        url:'http://api.ipinfodb.com/v3/ip-city/',
                        success:function(data)
                        {
                            var country = data.countryName;
                            var city = data.cityName;
                            nodehandle.html(nodehandle.html()+" - "+city+", "+country)
                        }
                    })
                });
            })
        </script>
        <meta charset="utf-8" />
    </head>
    <body>
<?php
            include('inc/env_vars.php');
            $link = mysql_connect(ENV_MAIN_MYSQL_HOST, ENV_MAIN_MYSQL_USER, ENV_MAIN_MYSQL_PASSWORD);
            if(!$link)
            {
                die('Couldn\'t esablish a connecion to the statistics databse.');
            }
            if(!mysqli_select_db('diggety_statistics', $link))
            {
                die('Couldn\'t select the statistics database. Reason: ' . mysql_error($link) . '');
            }
            $query = "SELECT COUNT(DISTINCT IPv4) AS 'uniques', COUNT(*) AS 'total' FROM statistics";
            $result = mysql_query($query, $link);
            if(!$result)
            {
                die('Couldn\' read the statistics from the statistics database. Reason: ' . mysql_error($link) . '');
            }
            else
            {
                $row = mysql_fetch_assoc($result);
                echo "        <h2>Uniques: " . $row['uniques'] . "</h2>\n";
                echo "        <h2>Visits: " . $row['total'] . "</h2>\n";
            }

            $query = "SELECT FROM_UNIXTIME(`timestamp`) as 'datetime', IPv4, IPv6, useragent, request_uri FROM statistics order by `timestamp` DESC";
            $result = mysql_query($query, $link);
            if(!$result)
            {
                die('Couldn\'t read the statistics from the statistics database. Reason: ' . mysql_error($link) . '');
            }
            else
            {
                echo "        <table cellpadding=\"5px\" cellspacing=\"0px\">\n";
                echo "            <tr>\n";
                echo "                <th>\n";
                echo "                \tDate & Time\n";
                echo "                </th>\n";
                echo "                <th>\n";
                echo "                \tIPv4\n";
                echo "                </th>\n";
                echo "                <th>\n";
                echo "                \tIPv6\n";
                echo "                </th>\n";
                echo "                <th>\n";
                echo "                \tUser Agent\n";
                echo "                </th>\n";
                echo "                <th>\n";
                echo "                \tRequest URI\n";
                echo "                </th>\n";
                echo "            </tr>\n";
                $bool = true;
                while($row = mysql_fetch_assoc($result))
                {
                    if($bool)
                    {
                        echo "            <tr style=\"background-color:lightblue;\">\n";
                    }
                    else
                    {
                        echo "            <tr>\n";
                    }
                    echo "                <td>\n";
                    echo "                    " . $row['datetime'] . "\n";
                    echo "                </td>\n";
                    echo "                <td>\n";
                    echo "                    <a href=\"javascript:void(0)\">" . $row['IPv4'] . "</a>\n";
                    echo "                </td>\n";
                    echo "                <td>\n";
                    echo "                    " . $row['IPv6'] . "\n";
                    echo "                </td>\n";
                    echo "                <td>\n";
                    echo "                    " . $row['useragent'] . "\n";
                    echo "                </td>\n";
                    echo "                <td>\n";
                    echo "                    " . $row['request_uri'] . "\n";
                    echo "                </td>\n";
                    echo "            </tr>\n";
                    $bool = !$bool;
                }
                echo "        </table>\n";
                echo "        Statistics successfully loaded.\n";
            }
        ?>
    </body>
</html>